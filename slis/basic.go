package slis

// 泛型: 判断切片中是否包含某个元素
func In[T comparable](elems []T, val T) bool {
	for _, v := range elems {
		if v == val {
			return true
		}
	}
	return false
}
