package constant

/*
 * 类型: 启用状态
 */
const (
	STATUS_DISABLE, DISABLE uint8 = iota, iota
	STATUS_ENABLE, ENABLE
)

/*
 * 类型: 状态开关
 */
const (
	SWITCH_OFF, OFF uint8 = iota, iota
	SWITCH_ON, ON
)

/*
 * 类型: 性别
 */
type TGender uint8

const (
	GENDER_UNKNOWN TGender = iota // 未知
	GENDER_FEMALE                 // 男
	GENDER_MALE                   // 女
)

/*
 * 类型: 内存容量
 */
type TMemSize uint64

const (
	MEM_SIZE_BYTE, BYTE TMemSize = 1 << (iota * 10), 1 << (iota * 10)
	MEM_SIZE_KB, KB
	MEM_SIZE_MB, MB
	MEM_SIZE_GB, GB
	MEM_SIZE_TB, TB
	MEM_SIZE_PB, PB
)
