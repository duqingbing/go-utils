package basic

// key 为字符串的 map
type KMap map[string]interface{}

// 空 结构体类型
type EStruct = struct{}

// key 为字符串， value 为空结构的 map
type KStructMap map[string]EStruct

// 将字符串切片转换成值为空的 map
func (m KStructMap) ToSlice() KSlice {
	s := KSlice{}
	for k, _ := range m {
		s = append(s, k)
	}
	return s
}

// string 类型的切片
type KSlice []string

// 将字符串切片转换成值为空的 map
func (s KSlice) ToMap() KStructMap {
	m := make(KStructMap, 0)
	for _, v := range s {
		m[v] = struct{}{}
	}
	return m
}
