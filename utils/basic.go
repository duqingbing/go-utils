package utils

import (
	"reflect"
)

// 判断变量是否零值
func IsZero(v interface{}) bool {
	return reflect.DeepEqual(v, reflect.Zero(reflect.TypeOf(v)).Interface())
}

// 判断变量是否是 nil
func IsNil(v interface{}) bool {
	if v == nil {
		return true
	}
	if !IsPtr(v) {
		return false
	}
	vo := reflect.ValueOf(v)
	if vo.IsNil() {
		return true
	}
	return false
}

// 判断变量是否是结构体
func IsStruct(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Struct)
}

// 判断变量是否是指针
func IsPtr(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr)
}

// 判断变量类型是否为 reflect.Value
func IsReflectValue(v interface{}) bool {
	_, ok := v.(reflect.Value)
	return ok
}

// 判断变量是否是结构体指针
func IsStructPtr(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).Elem().Kind() == reflect.Struct)
}

/**
 * 通过 If 函数模拟实现 go 的三元操作符
 */
func IF[T comparable](cond bool, trueVal, falseVal T) T {
	if cond {
		return trueVal
	}
	return falseVal
}

/**
 * 通过 bool 表达式判断是否 panic
 */
func Assert(express bool, text string) {
	if !express {
		panic(text)
	}
}
