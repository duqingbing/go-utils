package formats

import "time"

// 自定义和时间格式化
func FormatDate(t time.Time, format string) string {
	return t.Format(format)
}

// 格式化成日期
func Date(args ...any) string {
	if len(args) == 0 {
		return time.Now().Format("2006-01-02")
	}
	switch args[0].(type) {
	case time.Time:
		return args[0].(time.Time).Format("2006-01-02")
	case string:
		return time.Now().Format(args[0].(string))
	default:
		return time.Now().Format("2006-01-02")
	}
}

// 格式化成日期时间
func Datetime(args ...any) string {
	if len(args) == 0 {
		return time.Now().Format("2006-01-02 15:04:05")
	}
	switch args[0].(type) {
	case time.Time:
		return args[0].(time.Time).Format("2006-01-02 15:04:05")
	case string:
		return time.Now().Format(args[0].(string))
	default:
		return time.Now().Format("2006-01-02 15:04:05")
	}
}
