package runtimes

import (
	"reflect"
	"runtime"
)

// 获取运行中函数名
func NameOfFunc(f any) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}

// 获取运行中的调用信息
func Caller(skip ...int) (pc uintptr, file string, line int, ok bool) {
	if len(skip) > 0 {
		return runtime.Caller(skip[0])
	}
	return runtime.Caller(1)
}
