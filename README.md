# go 语言通用函数包

- [go 语言通用函数包](#go-语言通用函数包)
  - [1. 下载](#1-下载)
  - [2. 支持函数](#2-支持函数)
  - [3. 测试用例使用示例](#3-测试用例使用示例)

## 1. 下载

```bash
go get qingbing/xzutils
# 指定某个 commitId 进行拉取
go get -u qingbing/xzutils@commitId
```

## 2. 支持函数

- basic: 基础
  - 类型
    - `type KMap map[string]interface{}`: key-value 映射
    - `type EStruct = struct{}`: 空 结构体类型
    - `type KStructMap map[string]EStruct`: key 为字符串， value 为空结构的 map
      - `ToSlice()`: 转换成 KSlice
    - `type KSlice []string`: string 类型的切片
      - `ToMap()`: 转换成 KStructMap
    - `type XzTestingCase struct {Name string,Args KMap,Want KMap}`: 通用测试示例
  - 测试工具
    - `NewTesting()`: 单元测试工具
- constant: 常量
  - 启用状态
	- STATUS_DISABLE, DISABLE uint8: 0
	- STATUS_ENABLE, ENABLE uint8: 1
  - 状态开关
	- SWITCH_OFF, OFF uint8: 0
	- SWITCH_ON, ON uint8: 1
  - 性别
    - TGender uint8
    - GENDER_UNKNOWN TGender: 0
    - GENDER_FEMALE TGender: 1
    - GENDER_MALE TGender: 2
  - 内存容量
    - TGender uint64
    - MEM_SIZE_BYTE, BYTE TMemSize
    - MEM_SIZE_KB, KB TMemSize
    - MEM_SIZE_MB, MB TMemSize
    - MEM_SIZE_GB, GB TMemSize
    - MEM_SIZE_TB, TB TMemSize
    - MEM_SIZE_PB, PB TMemSize
- crypt: 编码
  - base64
    - `Base64Encode(v string) string`: base64 编码
    - `Base64Decode(v string) (str string, err error)`: base64 解码
  - json
    - `JsonEncode(data any) (string, error)`: json 编码
    - `JsonDecode(data string, obj any) error`: json 解码
  - md5
    - `func Md5(v string) string`: md5 编码
- debug: 调试
  - 栈
    - `func CallerStack(isPrint bool) string`: 获取或返回调用处的调用栈
- files: 文件操作
  - 目录操作
    - `FileList(dir string, ignores KSlice, recursion bool) (res []string, err error)`: 获取目录下的文件列表
    - `CopyDir(src, dist string, ignores KSlice) error`: 复制目录
    - `RmDir(dir string, rmSelf bool) error`: 删除目录
    - `FlushDir(dir string) error`: 清空目录下所有文件和目录
    - `ReadFile(filename string) (content []byte, err error)`: 读取文件内容
  - 文件操作
    - `GetStat(filename string) (fs.FileInfo, error)`: 获取文件信息
    - `GetExtension(filename string, extensions ...[]string) string`: 获取文件扩展名
    - `GetName(filename string, extensions ...string) string`: 获取文件名， 不带扩展
    - `CopyFile(src, dist string) (copySize int, err error)`: 复制文件
    - `CutFile(file string, extensions ...string) (string, error)`: 文件按照数字大小来切片并移动
    - `Create(filename string) (*os.File, error)`: 打开文件句柄(如果文件存在, 内容会被清空)
    - `Remove(filename string) error`: 删除文件
    - `Rename(oldPath string, newPath string) error`: 重命名(移动)文件
    - `OpenFile(filename string) (*os.File, error)`: 打开文件文件句柄
    - `PutBytesToWriter(fp *os.File, content []byte) (n int, err error)`: 将 []byte 追加到句柄
    - `PutStringToWriter(fp *os.File, content string) (n int, err error)`: 将 string 追加到句柄
    - `PutBytesToFile(filename string, content []byte) (int, error)`: 将 []byte 追加文件
    - `PutStringToFile(filename string, content string) (n int, err error)`: 将 string 追加文件
- formats: 格式化操作
  - `FormatDate(t time.Time, format string) string`: 自定义和时间格式化
  - `Date(args ...any) string`: 格式化成日期
  - `Datetime(args ...any) string`: 格式化成日期时间
- maps: map操作
  - basic
    - `In[K comparable, V any](m map[K]V, val V) bool`: 泛型: 判断 map 中是否包含某个值
    - `Keys[K comparable](m map[K]interface{}) []K`: 泛型: 获取 map 中所有的 key
    - `Exist[K comparable](m map[K]interface{}, key K) bool`: 泛型: 判断 map 中是否包含某个健
  - structure转换: mapstructure
    - `NewStructure() *structure`: map-structure 转换工具
- mix: 混合功能或工具
  - mixins
    - `Mixins(m map[string]interface{}, a ...interface{})`: map, struct 合并成一个 map 数据
  - store: 变量管理工具, 使用 map 存储
    - `func NewStore() *TStore`: 创建一个 map 存储器
      - 普通变量
        - `func (ms *TStore) GetAll() *map[string]interface{}`: 获取所有普通变量
        - `func (ms *TStore) GetKey(key string, defaultVal interface{}) interface{}`: 获取普通变量
        - `func (ms *TStore) SetKey(key string, val interface{}) *TStore`: 设置普通变量
        - `func (ms *TStore) SetKeyIfNotExist(key string, val interface{}) *TStore`: 设置普通变量， 如果存在则不设置
        - `func (ms *TStore) DelKey(key string) *TStore`: 根据 key 删除一个数据
        - `func (ms *TStore) Clear() *TStore`: 清空普通数据
      - map 变量
        - `func (ms *TStore) GetMapAll(mKey string) *map[string]interface{}`: 获取某个 map 的所有变量
        - `func (ms *TStore) GetMapKey(mKey string, key string, defaultVal interface{}) interface{}`: 获取 map 变量
        - `func (ms *TStore) mapInit(mKey string) *TStore`: 初始化 map 数据
        - `func (ms *TStore) SetMapKey(mKey string, key string, val interface{}) *TStore`: 设置 map 变量
        - `func (ms *TStore) SetMapKeyIfNotExist(mKey string, key string, val interface{}) *TStore`: 设置 map 变量， 如果存在则不设置
        - `func (ms *TStore) DelMapKey(mKey string, key string) *TStore`: 根据 key 删除 map 中的一个数据
        - `func (ms *TStore) MapClear(mKey string) *TStore`: 清空一个 map 数据
  - struct
    - `StructToMap(obj interface{}, tags ...string) map[string]interface{}`: 结构体根据 tag 转换成 map， tag 默认 json
- random: 随机
  - `var Random`: 时间构建随机种子
  - int, 整数
    - `Int(n int) int`: 获取 [0, n) 之间的整数
    - `Range(min, max int) int`: 获取 [min, max) 之间的整数
  - string
    - `String(length uint8, mask uint8) (str string)`: 获取随机字符串, 可通过掩码选中字符串, mask 7 => 1,2,4, mask 3 => 1,2, mask 5 => 1, 4, 掩码最大目前为 31
    - `Uniqid() string`: 根据 当前时间+服务IP+当前进程ID 计算一个唯一 id
- runtimes: 运行时
  - `NameOfFunc(f any) string`: 获取运行中函数名
  - `Caller(skip ...int) (pc uintptr, file string, line int, ok bool)`: 获取运行中的调用信息
- slis: 切片操作， 避免和内置报名 `slices` 重名， 使用 `slice` 前三字母
  - `In[T comparable](elems []T, val T) bool`: 泛型: 判断切片中是否包含某个元素
- strs: 字符串操作， 避免和内置报名 `strings` 重名， 使用 `string` 前三字母
  - `IsFirstUpper(v string) bool`: 首字母是否大写
  - `Hump(s string, firstUpper bool, sepS ...string) (r string)`: 按照驼峰法改造字符串
  - `Replace(s string, replaces map[string]any, wrap... bool) string`: 使用map的元素替换字符串中元素
  - `Join(separator string, strs ...string) string`: 连接字符串
- structs: 结构体
  - `GetName(value any) string`: 获取结构体名称
  - `SetFieldValue(binding interface{}, name string, value interface{}) error`: 为结构体的指定属性赋值
  - `ToStruct(binding interface{}, value interface{}) error`: 将 结构体 value 的值赋值给 结构体 binding
- times: 时间工具封装
  - `type FormatType string`
  - const 常量
	- YEAR   FormatType = "year"
	- MONTH  FormatType = "month"
	- DATE   FormatType = "date"
	- HOUR   FormatType = "hour"
	- MINUTE FormatType = "minute"
	- SECOND FormatType = "second"
  - `New(t ...time.Time) *Tool`: 新建时间工具
    - `func (t *Tool) FirstSecondOfWeek(weekday ...time.Weekday) *Tool`: 时间周上某天的第一秒
    - `func (t *Tool) FirstSecond(formatType ...FormatType) *Tool`: 获取指定时间类型的第一秒
    - `func (t *Tool) Add(d time.Duration) *Tool`: 增加时长
    - `func (t *Tool) AddSecond(num int) *Tool`: 增加秒数
    - `func (t *Tool) AddMinute(num int) *Tool`: 增加分钟数
    - `func (t *Tool) AddHour(num int) *Tool`: 增加小时
    - `func (t *Tool) AddDay(num int) *Tool`: 增加天数
    - `func (t *Tool) AddMonth(num int) *Tool`: 增加月份
    - `func (t *Tool) AddSeason(num int) *Tool`: 增加季度
    - `func (t *Tool) AddYear(num int) *Tool`: 增加年份
    - `func (t *Tool) AddWeek(num int) *Tool`: 增加周
- tools: 功能或工具包
  - list， 队列
    - `NewList[T any]() XzList[T]`: 队列
  - parent_tree, 树形, 操作的是二维 map
    - `NewParentTree() *TreeMap`: 创建 tree-map 工具
      - `SetData(data []map[string]any) *TreeMap`: 设置原始数据
      - `SetIdFlag(flag string) *TreeMap`: 设置主 id 字段名
      - `SetPidFlag(flag string) *TreeMap`: 设置上级 id 字段名
      - `SetRootVal(val any) *TreeMap`: 设置顶级标志值
      - `GetTreeData() []TreeData`: 获取树形结构数据
  - time， 时间
    - `Time`: 类型，已指定 json.Unmarshal 和 json.Marshal 的处理方式， 可结合 grom 使用
    - `Now() Time`: 当时时间的 xztime.Time
    - `Parse(layout, value string) (time Time, err error)`: 根据字符及格式解析成 xztime.Time
- types: 一些基础类型
  - `DiscardWriter`: 无操作的 Writer 实现
- utils: 基础工具包
  - basic, 辅助函数
    - `IsZero(v interface{}) bool`: 判断一个变量是否零值
    - `IsNil(v interface{}) bool`: 判断变量是否是 nil
    - `IsStruct(v interface{}) bool`: 判断变量是否是结构体
    - `IsPtr(v interface{}) bool`: 判断变量是否是指针
    - `IsReflectValue(v interface{}) bool`: 判断变量类型是否为 reflect.Value
    - `IsStructPtr(v interface{}) bool`: 判断变量是否是结构体指针
    - `IF[T comparable](cond bool, trueVal, falseVal T) T`: 通过 If 函数模拟实现 go 的三元操作符
  - ip
    - `ServerIp() (ip string)`: 服务端 IP
    - `HasLocalIPAddr(ip string) bool`: 检测 IP 地址字符串是否是内网地址
    - `ClientIP(r *http.Request) string`: 获取客户端 IP
    - `ClientPublicIP(r *http.Request) string`: 获取客户端公网 IP
    - `RemoteIP(r *http.Request) string`: 通过 RemoteAddr 获取 IP 地址, 快速解析方法, 不一定准确
    - `IPString2Long(ip string) (uint, error)`: ip 字符串转为数值
    - `Long2IPString(i uint) (string, error)`: 把数值转为 ip 字符串
  - ref, 反射
    - `GetDefaultValue(typ reflect.Type) any`: 获取类型的默认值
    - `GetReflectValue(v interface{}) reflect.Value`: 获取变量的 reflect.Value
    - `GetReflectType(v interface{}) reflect.Type`: 获取变量的 reflect.Type
    - `CallRefValue(refV reflect.Value, args ...interface{}) (res []reflect.Value, err error)`: 可执行 ReflectValue 的调用
    - `CallFunc(obj any, args ...interface{}) (res []reflect.Value, err error)`: 函数的调用，参数的使用参考： CallRefValue
    - `CallMethod(obj any, method string, args ...interface{}) (res []reflect.Value, err error)`: 对象中方法的调用，参数的使用参考： CallRefValue
  - time_counter, 计时器
    - `NewTimeCounter() timeCounter`: 创建计时器容器
      - `Begin(flag string)`: 开始一个计时器
      - `End(flag string) float64`: 结束一个计时器
  - transfor, 进制转换
    - `FillWidth(str string, width int) string`: 数据前位 0 补足到指定宽度的倍数
    - `HexToBinary(hex string) (bin string, err error)`: 十六进制 转换成 二进制
    - `BinaryToHex(bin string) (hex string, err error)`: 二进制 转换成 十六进制
    - `StringToHex(str string) (bin string)`: 字符串 转换成 十六进制
    - `HexToString(str string) (string, error)`: 十六进制 转换成 字符串
    - `StringToBinary(str string) (string, error)`: 字符串 转换成 二进制
    - `BinaryToString(bin string) (string, error)`: 二进制 转换成 字符串

## 3. 测试用例使用示例

```go
package goutils

import (
	"testing"
)

func sum(a, b int) int {
	return a + b
}

func TestTesting(t *testing.T) {
	NewTesting().
		SetCases([]XzTestingCase{
			{
				Name: "1+2",
				Args: KMap{"a": 1, "b": 2},
				Want: KMap{"res": 3},
			},
			{
				Name: "2+2=4",
				Args: KMap{"a": 2, "b": 2},
				Want: KMap{"res": 4},
			},
		}).
		SetLogic(func(args KMap) KMap {
			res := make(KMap, 0)
			res["res"] = sum(args["a"].(int), args["b"].(int))
			return res
		}).
		Done(t)
}
```