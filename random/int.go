package random

import (
	"math/rand"
	"time"
)

// 启动初始化时设置随机种子
var Random *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

// 获取随机整数,最大不超过 max
func Int(n int) int {
	return Random.Intn(n)
}

// 获取范围 [ min, max ] 中的随机整数, min 和 max 需要先大小后大
func Range(min, max int) int {
	if min == max {
		return min
	}
	return Random.Intn(max-min) + min
}
