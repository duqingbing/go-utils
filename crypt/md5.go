package crypt

import (
	"crypto/md5"
	"encoding/hex"
)

// md5 编码
func Md5(v string) string {
	d := []byte(v)
	m := md5.New()
	m.Write(d)
	return hex.EncodeToString(m.Sum(nil))
}
