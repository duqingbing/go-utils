package crypt

import (
	"encoding/json"
)

// json 编码
func JsonEncode(data any) (string, error) {
	bs, err := json.Marshal(data)
	if err != nil {
		return "", err
	} else {
		return string(bs), nil
	}
}

// json 解码
func JsonDecode(data string, obj any) error {
	return json.Unmarshal([]byte(data), obj)
}
