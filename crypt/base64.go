package crypt

import "encoding/base64"

// base64 编码
func Base64Encode(v string) string {
	return base64.StdEncoding.EncodeToString([]byte(v))
}

// base64 解码
func Base64Decode(v string) (str string, err error) {
	bs, err := base64.StdEncoding.DecodeString(v)
	str = string(bs)
	return
}
