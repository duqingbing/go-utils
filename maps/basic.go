package maps

import (
	"reflect"
)

// 泛型: 判断 map 中是否包含某个值
func In[K comparable, V any](m map[K]V, val V) bool {
	if m == nil {
		return false
	}
	for _, v := range m {
		if reflect.DeepEqual(v, val) {
			return true
		}
	}
	return false
}

// 泛型: 判断 map 中是否包含某个健
func Exist[K comparable](m map[K]interface{}, key K) bool {
	if m == nil {
		return false
	}
	if _, has := m[key]; has {
		return true
	}
	return false
}

// 泛型: 获取 map 中所有的 key
func Keys[K comparable](m map[K]interface{}) []K {
	if m == nil {
		return []K{}
	}
	keys := make([]K, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}
