package mix

import "qingbing/xzutils/utils"

// map + struct 混入成 map
func Mixins(m map[string]interface{}, a ...interface{}) {
	for _, other := range a {
		switch other.(type) {
		case map[string]interface{}:
			for k, v := range other.(map[string]interface{}) {
				m[k] = v
			}
		case *map[string]interface{}:
			for k, v := range *(other.(*map[string]interface{})) {
				m[k] = v
			}
		default:
			if other != nil && utils.IsStruct(other) || utils.IsStructPtr(other) {
				Mixins(m, StructToMap(other))
			}
		}
	}
}
