package mix

type TStore struct {
	// 存储 key-value 数据
	data *map[string]interface{}
	// 存储 key-map 数据
	mData *map[string]interface{}
}

// 创建一个 map 存储器
func NewStore() *TStore {
	return &TStore{
		data:  &map[string]interface{}{},
		mData: &map[string]interface{}{},
	}
}

// 获取所有普通变量
func (ms *TStore) GetAll() *map[string]interface{} {
	return ms.data
}

// 获取普通变量
func (ms *TStore) GetKey(key string, defaultVal interface{}) interface{} {
	if v, ok := (*ms.data)[key]; ok {
		return v
	}
	return defaultVal
}

// 设置普通变量
func (ms *TStore) SetKey(key string, val interface{}) *TStore {
	(*ms.data)[key] = val
	return ms
}

// 设置普通变量， 如果存在则不设置
func (ms *TStore) SetKeyIfNotExist(key string, val interface{}) *TStore {
	if _, ok := (*ms.data)[key]; !ok {
		return ms.SetKey(key, val)
	}
	return ms
}

// 根据 key 删除一个数据
func (ms *TStore) DelKey(key string) *TStore {
	delete(*ms.data, key)
	return ms
}

// 清空普通数据
func (ms *TStore) Clear() *TStore {
	ms.data = &map[string]interface{}{}
	return ms
}

// 获取某个 map 的所有变量
func (ms *TStore) GetMapAll(mKey string) *map[string]interface{} {
	if v, ok := (*ms.mData)[mKey]; ok {
		return v.(*map[string]interface{})
	}
	return &map[string]interface{}{}
}

// 获取 map 变量
func (ms *TStore) GetMapKey(mKey string, key string, defaultVal interface{}) interface{} {
	m := ms.GetMapAll(mKey)

	if v, ok := (*m)[key]; ok {
		return v
	}
	return defaultVal
}

// 初始化 map 数据
func (ms *TStore) mapInit(mKey string) *TStore {
	if _, ok := (*ms.mData)[mKey]; !ok {
		(*ms.mData)[mKey] = &map[string]interface{}{}
	}
	return ms
}

// 设置 map 变量
func (ms *TStore) SetMapKey(mKey string, key string, val interface{}) *TStore {
	ms.mapInit(mKey)

	m := ms.GetMapAll(mKey)
	(*m)[key] = val
	return ms
}

// 设置 map 变量， 如果存在则不设置
func (ms *TStore) SetMapKeyIfNotExist(mKey string, key string, val interface{}) *TStore {
	ms.mapInit(mKey)

	m := ms.GetMapAll(mKey)
	if _, ok := (*m)[key]; !ok {
		return ms.SetMapKey(mKey, key, val)
	}
	return ms
}

// 根据 key 删除 map 中的一个数据
func (ms *TStore) DelMapKey(mKey string, key string) *TStore {
	ms.mapInit(mKey)

	m := ms.GetMapAll(mKey)
	delete(*m, key)
	return ms
}

// 清空一个 map 数据
func (ms *TStore) MapClear(mKey string) *TStore {
	if _, ok := (*ms.mData)[mKey]; ok {
		delete(*ms.mData, mKey)
	}
	return ms
}
