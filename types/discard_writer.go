package types

/**
 * 无操作的 Writer 实现
 *
 * 用于在需要 Writer 但不希望接收任何数据时使用。
 * 例如，在需要一个 Writer 但不希望输出到任何地方时，可以创建一个 DiscardWriter 并将其作为参数。
 */
type DiscardWriter struct {
}

/**
 * 实现 io.Writer 接口
 *
 * 忽略并丢弃所有数据，返回 0 和 nil 错误
 *
 * @param p 要忽略的数据
 * @return 忽略的数据长度和 nil 错误
 */
func (dw *DiscardWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}
