package test

import (
	"fmt"
	"qingbing/xzutils/times"
	"testing"
	"time"
)

func Test_Tool_FirstSecond(t *testing.T) {
	now := times.New()
	tomorrow := times.New(time.Now().Add(time.Hour * 25))

	ts := map[string]*times.Tool{
		"now":      now,
		"tomorrow": tomorrow,
	}
	for name, tt := range ts {
		fmt.Printf("=========== start  ===========\n")
		fmt.Printf("%-10s: %v\n", name, tt)
		fmt.Printf("----------- FirstSecondOfWeek  -----------\n")
		fmt.Printf("%-15s: %v\n", "Default", tt.FirstSecondOfWeek())
		fmt.Printf("%-15s: %v\n", "Sunday", tt.FirstSecondOfWeek(time.Sunday))
		fmt.Printf("%-15s: %v\n", "Monday", tt.FirstSecondOfWeek(time.Monday))
		fmt.Printf("%-15s: %v\n", "Tuesday", tt.FirstSecondOfWeek(time.Tuesday))
		fmt.Printf("%-15s: %v\n", "Wednesday", tt.FirstSecondOfWeek(time.Wednesday))
		fmt.Printf("%-15s: %v\n", "Thursday", tt.FirstSecondOfWeek(time.Thursday))
		fmt.Printf("%-15s: %v\n", "Friday", tt.FirstSecondOfWeek(time.Friday))
		fmt.Printf("%-15s: %v\n", "Saturday", tt.FirstSecondOfWeek(time.Saturday))

		fmt.Printf("----------- FirstSecond  -----------\n")
		fmt.Printf("%-15s: %v\n", "YEAR", tt.FirstSecond(times.YEAR))
		fmt.Printf("%-15s: %v\n", "MONTH", tt.FirstSecond(times.MONTH))
		fmt.Printf("%-15s: %v\n", "DATE", tt.FirstSecond(times.DATE))
		fmt.Printf("%-15s: %v\n", "DEFAULT", tt.FirstSecond())
		fmt.Printf("%-15s: %v\n", "HOUR", tt.FirstSecond(times.HOUR))
		fmt.Printf("%-15s: %v\n", "MINUTE", tt.FirstSecond(times.MINUTE))
		fmt.Printf("%-15s: %v\n", "SECOND", tt.FirstSecond(times.SECOND))

		fmt.Printf("----------- Add  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "- 1hour", tt.Add(-time.Hour))
		fmt.Printf("%-10s: %v\n", " 1hour", tt.Add(time.Hour))

		fmt.Printf("----------- AddSecond  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddSecond(-2))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddSecond(2))

		fmt.Printf("----------- AddMinute  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddMinute(-2))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddMinute(2))

		fmt.Printf("----------- AddHour  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddHour(-2))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddHour(2))

		fmt.Printf("----------- AddDay  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddDay(-2))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddDay(2))

		fmt.Printf("----------- AddMonth  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddMonth(-24))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddMonth(24))

		fmt.Printf("----------- AddSeason  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddSeason(-1))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddSeason(1))

		fmt.Printf("----------- AddYear  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddYear(-1))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddYear(1))

		fmt.Printf("----------- AddWeek  -----------\n")
		fmt.Printf("%-10s: %v\n", "cur", tt)
		fmt.Printf("%-10s: %v\n", "-2", tt.AddWeek(-1))
		fmt.Printf("%-10s: %v\n", " 2", tt.AddWeek(1))

		fmt.Printf("=========== end  ===========\n\n\n")
	}
}
