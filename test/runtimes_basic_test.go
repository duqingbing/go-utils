package test

import (
	"fmt"
	"qingbing/xzutils/basic"
	"qingbing/xzutils/runtimes"
	"testing"
)

// 辅助资源
type testNameOfFunc struct {
	// test-name: test.testNameOfFunc.test-fm
}

func (testNameOfFunc) test() {

}

type testNameOfFunc1 struct {
	// test-name: test.testNameOfFunc.test-fm
	testNameOfFunc
}

// 单元测试
func Test_Runtimes_NameOfFunc(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "运行中函数名称获取",
				Args: map[string]any{
					"func": runtimes.NameOfFunc,
				},
				Want: map[string]any{
					"out": "qingbing/xzutils/runtimes.NameOfFunc",
				},
			},
			{
				Name: "运行中结构体名称获取",
				Args: map[string]any{
					"func": testNameOfFunc{}.test,
				},
				Want: map[string]any{
					"out": "qingbing/xzutils/test.testNameOfFunc.test-fm",
				},
			},
			{
				Name: "运行中结构体名称获取",
				Args: map[string]any{
					"func": testNameOfFunc1{}.test,
				},
				Want: map[string]any{
					"out": "qingbing/xzutils/test.testNameOfFunc.test-fm",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = runtimes.NameOfFunc(args["func"])
			return res
		}).
		Done(t)
}

// 单元测试
func Test_Runtimes_Caller(t *testing.T) {
	up, file, line, ok := runtimes.Caller(0)
	if !ok {
		fmt.Println("无法获取调用者信息: ", "runtimes.Caller(0)")
	} else {
		fmt.Println("=== 调用者信息: ", "runtimes.Caller(0) ===")
		fmt.Printf("指针 : %v\n", up)
		fmt.Printf("文件 : %v\n", file)
		fmt.Printf("行号 : %v\n", line)
	}

	up, file, line, ok = runtimes.Caller()
	if !ok {
		fmt.Println("无法获取调用者信息: ", "runtimes.Caller()")
	} else {
		fmt.Println("=== 调用者信息: ", "runtimes.Caller() ===")
		fmt.Printf("指针 : %v\n", up)
		fmt.Printf("文件 : %v\n", file)
		fmt.Printf("行号 : %v\n", line)
	}

	up, file, line, ok = runtimes.Caller(1)
	if !ok {
		fmt.Println("无法获取调用者信息: ", "runtimes.Caller(1)")
	} else {
		fmt.Println("=== 调用者信息: ", "runtimes.Caller(1) ===")
		fmt.Printf("指针 : %v\n", up)
		fmt.Printf("文件 : %v\n", file)
		fmt.Printf("行号 : %v\n", line)
	}

	up, file, line, ok = runtimes.Caller(2)
	if !ok {
		fmt.Println("无法获取调用者信息: ", "runtimes.Caller(2)")
	} else {
		fmt.Println("=== 调用者信息: ", "runtimes.Caller(2) ===")
		fmt.Printf("指针 : %v\n", up)
		fmt.Printf("文件 : %v\n", file)
		fmt.Printf("行号 : %v\n", line)
	}
}
