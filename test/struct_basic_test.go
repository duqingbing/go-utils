package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/structs"
)

func Test_Basic_GetName(t *testing.T) {
	type TestInfo struct {
		Age int
		sex string
	}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "结构体名称",
				Args: map[string]any{
					"obj": TestInfo{},
				},
				Want: map[string]any{
					"out": "TestInfo",
				},
			},
			{
				Name: "指针结构体名称",
				Args: map[string]any{
					"obj": &TestInfo{},
				},
				Want: map[string]any{
					"out": "TestInfo",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = structs.GetName(args["obj"])
			return res
		}).
		Done(t)
}

func Test_Basic_SetValueByName(t *testing.T) {
	type Info struct {
		Age int
		sex string
	}

	type Person struct {
		Name     string
		Username string `set:"-"`
		Info
	}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "FieldName 不存在",
				Args: map[string]any{
					"binding":   &Person{},
					"fieldName": "Username",
					"value":     "qing",
				},
				Want: map[string]any{
					"out": &Person{},
				},
			},
			{
				Name: "类型不同",
				Args: map[string]any{
					"binding":   &Person{},
					"fieldName": "Name",
					"value":     1,
				},
				Want: map[string]any{
					"out": &Person{},
				},
			},
			{
				Name: "设置第一层数据",
				Args: map[string]any{
					"binding":   &Person{},
					"fieldName": "Name",
					"value":     "qing",
				},
				Want: map[string]any{
					"out": &Person{
						Name: "qing",
					},
				},
			},
			{
				Name: "设置第二层数据",
				Args: map[string]any{
					"binding":   &Person{},
					"fieldName": "Age",
					"value":     10,
				},
				Want: map[string]any{
					"out": &Person{
						Info: Info{
							Age: 10,
						},
					},
				},
			},
			{
				Name: "FieldName 忽略设置",
				Args: map[string]any{
					"binding":   &Person{},
					"fieldName": "Username",
					"value":     "qing",
				},
				Want: map[string]any{
					"out": &Person{},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			structs.SetFieldValue(args["binding"], args["fieldName"].(string), args["value"])
			res["out"] = args["binding"]
			return res
		}).
		Done(t)
}

func Test_Basic_ToStruct(t *testing.T) {
	type OB struct {
		Name string
	}
	type OC struct {
		Name int
	}
	type OD struct {
		Age int
	}
	type OE struct {
		Age string
	}
	type OF struct {
		Name string
		Sex  int
		Info map[string]interface{}
	}

	type OAAge struct {
		Age int
	}
	type OA struct {
		Name string
		Info map[string]interface{}
		OAAge
	}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "绑定值不是指针",
				Args: map[string]any{
					"binding": OA{},
					"value":   OB{},
				},
				Want: map[string]any{
					"out":      OA{},
					"hasError": true,
				},
			},
			{
				Name: "源数据不是结构体",
				Args: map[string]any{
					"binding": &OA{},
					"value":   "",
				},
				Want: map[string]any{
					"out":      &OA{},
					"hasError": true,
				},
			},
			{
				Name: "源数据为结构体且类型相同",
				Args: map[string]any{
					"binding": &OA{},
					"value":   OB{Name: "qingbing"},
				},
				Want: map[string]any{
					"out":      &OA{Name: "qingbing"},
					"hasError": false,
				},
			},
			{
				Name: "源数据为结构体但类型不同",
				Args: map[string]any{
					"binding": &OA{},
					"value":   OC{Name: 1111},
				},
				Want: map[string]any{
					"out":      &OA{},
					"hasError": false,
				},
			},
			{
				Name: "源数据为内嵌结构体且类型相同",
				Args: map[string]any{
					"binding": &OA{},
					"value":   OD{Age: 111},
				},
				Want: map[string]any{
					"out":      &OA{OAAge: OAAge{Age: 111}},
					"hasError": false,
				},
			},
			{
				Name: "源数据为结构体但类型不同",
				Args: map[string]any{
					"binding": &OA{},
					"value":   OE{Age: "111"},
				},
				Want: map[string]any{
					"out":      &OA{},
					"hasError": false,
				},
			},
			{
				Name: "源数据map形式",
				Args: map[string]any{
					"binding": &OA{},
					"value": OF{
						Name: "qingbing",
						Info: map[string]interface{}{
							"name": "qingbing",
						},
						Sex: 18,
					},
				},
				Want: map[string]any{
					"out": &OA{
						Name: "qingbing",
						Info: map[string]interface{}{
							"name": "qingbing",
						},
					},
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			err := structs.ToStruct(args["binding"], args["value"])
			fmt.Printf("== err: %+v === binding: %+v ===\v", err, args["binding"])
			if err != nil {
				res["hasError"] = true
				res["out"] = args["binding"]
			} else {
				res["hasError"] = false
				res["out"] = args["binding"]
			}
			return res
		}).
		Done(t)
}
