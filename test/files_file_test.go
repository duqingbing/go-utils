package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/files"
	"qingbing/xzutils/runtimes"
)

func Test_Files_GetStat(t *testing.T) {
	// 获取调用者信息（在这个例子中是main函数）
	_, filename, _, _ := runtimes.Caller(1)
	info, err := files.GetStat(filename)
	fmt.Printf(" err: %v\n", err)
	fmt.Printf("info:\n")
	fmt.Printf("     Name: %#v\n", info.Name())
	fmt.Printf("     Size: %#v\n", info.Size())
	fmt.Printf("     Mode: %#v\n", info.Mode())
	fmt.Printf("  ModTime: %#v\n", info.ModTime())
	fmt.Printf("    IsDir: %#v\n", info.IsDir())
	fmt.Printf("      Ext: %#v\n", info.Ext())
	fmt.Printf("      Dir: %#v\n", info.Dir())
	fmt.Printf(" Filename: %#v\n", info.Filename())
	fmt.Printf("      Sys: %#v\n", info.Sys())
}

func Test_Files_GetExtension(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通文件",
				Args: map[string]any{
					"filename": "xx.txt",
					"exts":     []string{},
				},
				Want: map[string]any{
					"ext": ".txt",
				},
			},
			{
				Name: "普通文件不指定特别后缀",
				Args: map[string]any{
					"filename": "xxx.tar.gz",
					"exts":     []string{},
				},
				Want: map[string]any{
					"ext": ".gz",
				},
			},
			{
				Name: "tar.gz 文件",
				Args: map[string]any{
					"filename": "xxx.tar.gz",
					"exts":     []string{".tar.gz"},
				},
				Want: map[string]any{
					"ext": ".tar.gz",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["ext"] = files.GetExtension(args["filename"].(string), args["exts"].([]string)...)
			return res
		}).
		Done(t)
}

func Test_Files_GetName(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通文件",
				Args: map[string]any{
					"filename": "./../test/xx.txt",
					"exts":     []string{},
				},
				Want: map[string]any{
					"ext": "xx",
				},
			},
			{
				Name: "普通文件不指定特别后缀",
				Args: map[string]any{
					"filename": "./../test/xxx.tar.gz",
					"exts":     []string{},
				},
				Want: map[string]any{
					"ext": "xxx.tar",
				},
			},
			{
				Name: "tar.gz 文件",
				Args: map[string]any{
					"filename": "./../test/xxx.tar.gz",
					"exts":     []string{".tar.gz"},
				},
				Want: map[string]any{
					"ext": "xxx",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["ext"] = files.GetName(args["filename"].(string), args["exts"].([]string)...)
			return res
		}).
		Done(t)
}

func Test_Files_ReadFile(t *testing.T) {
	file := "file_test.go"
	content, err := files.ReadFile(file)
	fmt.Println(string(content), err)
}

/*
func Test_Files_CopyFile(t *testing.T) {
	srcFile := "file_test.go"
	distFile := "file_test.out"
	size, err := files.CopyFile(srcFile, distFile)
	if err != nil {
		fmt.Println("复制文件失败")
	} else {
		fmt.Printf("成功复制了 %d 字节", size)
	}
}
*/

func Test_Files_Create(t *testing.T) {
	filename := "./../tmp/create_file.txt"
	fp, err := files.Create(filename)
	fmt.Printf("fp: %v\n", fp)
	fmt.Printf("err: %v\n", err)

	fp.WriteString("创建文件, 存在内容会被清空")
}
func Test_Files_Remove(t *testing.T) {
	filename := "./../tmp/create_file.txt"
	err := files.Remove(filename)
	fmt.Printf("err: %#v\n", err)
}

func Test_Files_Rename(t *testing.T) {
	oldFilename := "./../tmp/create_file.txt"
	newFilename := "./../tmp/create_file.txt"
	err := files.Rename(oldFilename, newFilename)
	fmt.Printf("err: %v\n", err)
	fmt.Printf("err: %#v\n", err)
}

func Test_Files_OpenFile(t *testing.T) {
	filename := "./../tmp/open_file.txt"
	fp, err := files.OpenFile(filename)
	fmt.Printf("fp: %v\n", fp)
	fmt.Printf("err: %v\n", err)
}

func Test_Files_PutBytesToWriter(t *testing.T) {
	filename := "./../tmp/put_byte_writer.txt"
	fp, _ := files.OpenFile(filename)
	num, err := files.PutBytesToWriter(fp, []byte("bytes content\n"))
	fmt.Printf("num: %v\n", num)
	fmt.Printf("err: %v\n", err)
}

func Test_Files_PutStringToWriter(t *testing.T) {
	filename := "./../tmp/put_string_writer.txt"
	fp, _ := files.OpenFile(filename)
	num, err := files.PutStringToWriter(fp, "string content\n")
	fmt.Printf("num: %v\n", num)
	fmt.Printf("err: %v\n", err)
}

func Test_Files_PutBytesToFile(t *testing.T) {
	filename := "./../tmp/put_byte_file.txt"
	num, err := files.PutBytesToFile(filename, []byte("bytes content\n"))
	fmt.Printf("num: %v\n", num)
	fmt.Printf("err: %v\n", err)
}

func Test_Files_PutStringToFile(t *testing.T) {
	filename := "./../tmp/put_string_file.txt"
	num, err := files.PutStringToFile(filename, "string content\n")
	fmt.Printf("num: %v\n", num)
	fmt.Printf("err: %v\n", err)
}

func Test_Files_CutFile(t *testing.T) {
	// filename := "./../tmp/cut_file.tar.gz"
	filename := "./../tmp/cut_file.txt"
	newFile, err := files.CutFile(filename)
	fmt.Printf("newFile: %v\n", newFile)
	fmt.Printf("err    : %v\n", err)
}
