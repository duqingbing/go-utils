package test

import (
	"fmt"
	"qingbing/xzutils/utils"
	"testing"
	"time"
)

func Test_Utils_TimeCounter(t *testing.T) {
	tc := utils.NewTimeCounter()
	tc.Begin("yy")
	tc.Begin("xx")
	go func(tx *utils.TimeCounter) {
		tx.End("yy")
	}(&tc)
	time.Sleep(time.Second)
	tc.End("xx")
	time.Sleep(time.Second * 2)
	fmt.Println(tc.String())
}
