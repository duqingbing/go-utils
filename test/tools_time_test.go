package test

import (
	"encoding/json"
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/tools"
)

func TestTimeJsonEncode(t *testing.T) {
	td, _ := tools.Parse("2006-01-02 15:04:05", "2024-05-02 13:31:21")
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "json 编码: 单个 Time",
				Args: basic.KMap{"data": td},
				Want: basic.KMap{"res": "\"2024-05-02 13:31:21\"", "hasError": false},
			},
			{
				Name: "json 编码: map 混入 Time",
				Args: basic.KMap{"data": map[string]interface{}{"id": 5, "date": td}},
				Want: basic.KMap{"res": "{\"date\":\"2024-05-02 13:31:21\",\"id\":5}", "hasError": false},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			bs, err := json.Marshal(args["data"])
			if err == nil {
				res["hasError"] = false
				res["res"] = string(bs)
			} else {
				res["hasError"] = true
				res["res"] = nil
			}
			return res
		}).
		Done(t)
}

func TestTimeJsonDecode(t *testing.T) {
	var out1 tools.Time

	type test struct {
		id   float64
		date tools.Time
	}
	var out2 test

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "json 解码: 年",
				Args: basic.KMap{"data": "\"2024\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: 月",
				Args: basic.KMap{"data": "\"2024-05\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: 日",
				Args: basic.KMap{"data": "\"2024-05-02\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: 小时",
				Args: basic.KMap{"data": "\"2024-05-02 13\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: 分钟",
				Args: basic.KMap{"data": "\"2024-05-02 13:31\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: 单个 Time",
				Args: basic.KMap{"data": "\"2024-05-02 13:31:21\"", "out": &out1},
				Want: basic.KMap{"res": &out1, "hasError": false},
			},
			{
				Name: "json 解码: map 混入 Time",
				Args: basic.KMap{"data": "{\"date\":\"2024-05-02 13:31:21\",\"id\":5}", "out": &out2},
				Want: basic.KMap{"res": &out2, "hasError": false},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			err := json.Unmarshal([]byte(args["data"].(string)), args["out"])
			fmt.Printf("%#v\n", args["out"])
			if err == nil {
				res["hasError"] = false
				res["res"] = args["out"]
			} else {
				res["hasError"] = true
				res["res"] = nil
			}
			return res
		}).
		Done(t)
}

func TestNow(t *testing.T) {
	fmt.Println(tools.Now())
}
