package test

import (
	"fmt"
	"qingbing/xzutils/constant"
	"testing"
)

func Test_Constant_Status(t *testing.T) {
	fmt.Printf("STATUS_DISABLE: %T, %[1]d; DISABLE: %[2]T, %[2]d\n", constant.STATUS_DISABLE, constant.DISABLE)
	fmt.Printf("STATUS_ENABLE: %T, %[1]d; ENABLE: %[2]T, %[2]d\n", constant.STATUS_ENABLE, constant.ENABLE)
}

func Test_Constant_Switch(t *testing.T) {
	fmt.Printf("SWITCH_OFF: %T, %[1]d; OFF: %[2]T, %[2]d\n", constant.SWITCH_OFF, constant.OFF)
	fmt.Printf("SWITCH_ON: %T, %[1]d; ON: %[2]T, %[2]d\n", constant.SWITCH_ON, constant.ON)
}

func Test_Constant_GENDER(t *testing.T) {
	fmt.Printf("GENDER_FEMALE: %T, %[1]d\n", constant.GENDER_FEMALE)
	fmt.Printf("GENDER_MALE: %T, %[1]d\n", constant.GENDER_MALE)
	fmt.Printf("GENDER_UNKNOWN: %T, %[1]d\n", constant.GENDER_UNKNOWN)
}

func Test_Constant_MEMSIZE(t *testing.T) {
	fmt.Printf("MEM_SIZE_BYTE: %T, %[1]d BYTE: %[2]T, %[2]d\n", constant.MEM_SIZE_BYTE, constant.BYTE)
	fmt.Printf("MEM_SIZE_KB: %T, %[1]d; KB: %[2]T, %[2]d\n", constant.MEM_SIZE_KB, constant.KB)
	fmt.Printf("MEM_SIZE_MB: %T, %[1]d; MB: %[2]T, %[2]d\n", constant.MEM_SIZE_MB, constant.MB)
	fmt.Printf("MEM_SIZE_GB: %T, %[1]d; GB: %[2]T, %[2]d\n", constant.MEM_SIZE_GB, constant.GB)
	fmt.Printf("MEM_SIZE_TB: %T, %[1]d; TB: %[2]T, %[2]d\n", constant.MEM_SIZE_TB, constant.TB)
	fmt.Printf("MEM_SIZE_PB: %T, %[1]d; PB: %[2]T, %[2]d\n", constant.MEM_SIZE_PB, constant.PB)
}
