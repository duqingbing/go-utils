package test

import (
	"fmt"
	"qingbing/xzutils/basic"
	"qingbing/xzutils/utils"
	"reflect"
	"testing"
)

/**
 * 测试辅助函数
 */

func testRefFunc1(word string, ha string, good string) string {
	// fmt.Printf("word: %v\n", word)
	// fmt.Printf("ha: %v\n", ha)
	// fmt.Printf("good: %v\n", good)
	return fmt.Sprintf("%v-%v-%v", word, ha, good)
}

func testRefFunc2(hello, world string, args ...string) string {
	str := hello + "-" + world
	// fmt.Printf("hello: %v\n", hello)
	// fmt.Printf("world: %v\n", world)
	// fmt.Printf("args: %v\n", args)
	for i := 0; i < len(args); i++ {
		str += "-" + args[i]
		// fmt.Printf("arg[%v]: %v\n", i, args[i])
	}
	return str
}

type testRefObject struct {
}

func (c testRefObject) TestRefFunc1(word string, ha string, good string) string {
	return testRefFunc1(word, ha, good)
}

func (c testRefObject) TestRefFunc2(hello, world string, args ...string) string {
	return testRefFunc2(hello, world, args...)
}

/**
 * 单元测试
 */
func Test_Utils_GetDefaultValue(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "int 数字",
				Args: map[string]any{
					"val": (int)(222),
				},
				Want: map[string]any{
					"out": (int)(0),
				},
			},
			{
				Name: "int64 数字",
				Args: map[string]any{
					"val": (int64)(222),
				},
				Want: map[string]any{
					"out": (int64)(0),
				},
			},
			{
				Name: "string 字符串",
				Args: map[string]any{
					"val": "string",
				},
				Want: map[string]any{
					"out": "",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.GetDefaultValue(reflect.TypeOf(args["val"]))
			return res
		}).
		Done(t)
}

func Test_Utils_GetReflectVal(t *testing.T) {
	type x struct {
		Name string `json:"name"`
		Sex  string `json:"sex"`
	}

	obj := x{Name: "duqingbing", Sex: "male"}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "reflect value 结构体",
				Args: map[string]any{
					"val":     reflect.ValueOf(obj),
					"message": "\n====== 结构体 ======",
				},
				Want: map[string]any{},
			},
			{
				Name: "reflect value 结构体指针",
				Args: map[string]any{
					"val":     reflect.ValueOf(&obj),
					"message": "\n====== 结构体指针 ======",
				},
				Want: map[string]any{},
			},
			{
				Name: "结构体",
				Args: map[string]any{
					"val":     obj,
					"message": "\n====== 结构体 ======",
				},
				Want: map[string]any{},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"val":     &obj,
					"message": "\n====== 结构体指针 ======",
				},
				Want: map[string]any{},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			fmt.Println(args["message"])
			val := utils.GetReflectValue(args["val"])
			for i := 0; i < val.NumField(); i++ {
				field := val.Type().Field(i)
				key := field.Tag.Get("json")
				fmt.Printf("=== key: %v == val: %#v\n", key, val.Field(i).Interface())
			}
			return basic.KMap{}
		}).
		Done(t)
}

func Test_Utils_GetReflectType(t *testing.T) {
	type Info struct {
		Qq string `json:"qq"`
	}
	type X struct {
		Name string `json:"name"`
		Sex  string `json:"sex"`
		*Info
	}

	obj := X{Name: "duqingbing", Sex: "male"}
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "结构体",
				Args: map[string]any{
					"val":     obj,
					"message": "\n====== 结构体 ======",
				},
				Want: map[string]any{},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"val":     obj,
					"message": "\n====== 结构体指针 ======",
				},
				Want: map[string]any{},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			fmt.Println(args["message"])
			typ := utils.GetReflectType(args["val"])
			for i := 0; i < typ.NumField(); i++ {
				fmt.Printf("= Name: %s; Index: %v; Type: %v\n", typ.Field(i).Name, typ.Field(i).Index, typ.Field(i).Type)
			}
			return basic.KMap{}
		}).
		Done(t)
}

func Test_Utils_CallRefValue(t *testing.T) {
	var res []reflect.Value
	var err error
	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc1), "Hello", "my", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc1), "Hello", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc1), "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc2), "Hello", "My", "Baby", "Good")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc2), "Hello", "My", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc2), "Hello", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallRefValue(reflect.ValueOf(testRefFunc2), "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
}

func Test_Utils_CallFunc(t *testing.T) {
	var res []reflect.Value
	var err error
	res, err = utils.CallFunc(testRefFunc1, "Hello", "my", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallFunc(testRefFunc1, "Hello", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallFunc(testRefFunc1, "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallFunc(testRefFunc2, "Hello", "My", "Baby", "Good")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallFunc(testRefFunc2, "Hello", "My", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallFunc(testRefFunc2, "Hello", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallFunc(testRefFunc2, "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
}

func Test_Utils_CallMethod(t *testing.T) {
	var res []reflect.Value
	var err error
	obj := testRefObject{}
	res, err = utils.CallMethod(obj, "TestRefFunc1", "Hello", "my", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallMethod(obj, "TestRefFunc1", "Hello", "baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
	res, err = utils.CallMethod(obj, "TestRefFunc1", "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallMethod(obj, "TestRefFunc2", "Hello", "My", "Baby", "Good")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallMethod(obj, "TestRefFunc2", "Hello", "My", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallMethod(obj, "TestRefFunc2", "Hello", "Baby")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)

	res, err = utils.CallMethod(obj, "TestRefFunc2", "Hello")
	fmt.Printf("=== res: %v ===\n", res[0].Interface().(string))
	fmt.Printf("=== err: %#v ===\n\n", err)
}
