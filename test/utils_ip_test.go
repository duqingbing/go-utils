package test

import (
	"net"
	"net/http"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/utils"
)

func Test_Utils_HasLocalIPAddr(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "ip 字符串是否本地地址: 127.0.0.1",
				Args: map[string]any{
					"ip": "127.0.0.1",
				},
				Want: map[string]any{
					"isLocal": true,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 10.168.9.18",
				Args: map[string]any{
					"ip": "10.168.9.18",
				},
				Want: map[string]any{
					"isLocal": true,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 182.56.9.18",
				Args: map[string]any{
					"ip": "182.56.9.18",
				},
				Want: map[string]any{
					"isLocal": false,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 192.168.9.18",
				Args: map[string]any{
					"ip": "192.168.9.18",
				},
				Want: map[string]any{
					"isLocal": true,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 11.168.9.18",
				Args: map[string]any{
					"ip": "11.168.9.18",
				},
				Want: map[string]any{
					"isLocal": false,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 172.16.9.18",
				Args: map[string]any{
					"ip": "172.16.9.18",
				},
				Want: map[string]any{
					"isLocal": true,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 172.31.9.18",
				Args: map[string]any{
					"ip": "172.31.9.18",
				},
				Want: map[string]any{
					"isLocal": true,
				},
			},
			{
				Name: "ip 字符串是否本地地址: 172.32.9.18",
				Args: map[string]any{
					"ip": "172.32.9.18",
				},
				Want: map[string]any{
					"isLocal": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["isLocal"] = utils.HasLocalIPAddr(args["ip"].(string))
			return res
		}).
		Done(t)
}

func Test_Utils_RemoteIP(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "ip 字符串转换成数字: 101.1.0.4:100",
				Args: map[string]any{
					"ip": "101.1.0.4:100",
				},
				Want: map[string]any{
					"remoteIp": "101.1.0.4",
				},
			},
			{
				Name: "ip 字符串转换成数字: 101.1.0.4:",
				Args: map[string]any{
					"ip": "101.1.0.4:",
				},
				Want: map[string]any{
					"remoteIp": "101.1.0.4",
				},
			},
			{
				Name: "ip 字符串转换成数字: 101.1.0.4",
				Args: map[string]any{
					"ip": "101.1.0.4",
				},
				Want: map[string]any{
					"remoteIp": "",
				},
			},
			{
				Name: "ip 字符串转换成数字: :100",
				Args: map[string]any{
					"ip": ":100",
				},
				Want: map[string]any{
					"remoteIp": "",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["remoteIp"] = utils.RemoteIP(&http.Request{RemoteAddr: args["ip"].(string)})
			return res
		}).
		Done(t)
}

func Test_Utils_ClientIP(t *testing.T) {
	r := &http.Request{Header: http.Header{}}
	r.Header.Set("X-Real-IP", " 10.10.10.10  ")
	r.Header.Set("X-Forwarded-For", "  20.20.20.20, 30.30.30.30")
	r.RemoteAddr = "  40.40.40.40:42123 "

	if ip := utils.ClientIP(r); ip != "20.20.20.20" {
		t.Errorf("actual: 20.20.20.20, expected:%s", ip)
	}

	r.Header.Del("X-Forwarded-For")
	if ip := utils.ClientIP(r); ip != "10.10.10.10" {
		t.Errorf("actual: 10.10.10.10, expected:%s", ip)
	}

	r.Header.Set("X-Forwarded-For", "30.30.30.30  ")
	if ip := utils.ClientIP(r); ip != "30.30.30.30" {
		t.Errorf("actual: 30.30.30.30, expected:%s", ip)
	}

	r.Header.Del("X-Forwarded-For")
	r.Header.Del("X-Real-IP")
	if ip := utils.ClientIP(r); ip != "40.40.40.40" {
		t.Errorf("actual: 40.40.40.40, expected:%s", ip)
	}

	r.RemoteAddr = "50.50.50.50"
	if ip := utils.ClientIP(r); ip != "" {
		t.Errorf("ip: 50.50.50.50")
	}
}

func Test_Utils_ClientPublicIP(t *testing.T) {
	for _, v := range []struct {
		xForwardedFor string
		remoteAddr    string
		expected      string
	}{
		{"10.3.5.45, 21.45.9.1", "101.1.0.4:100", "21.45.9.1"},
		{"101.3.5.45, 21.45.9.1", "101.1.0.4:100", "101.3.5.45"},
		{"", "101.1.0.4:100", "101.1.0.4"},
		{"21.45.9.1", "101.1.0.4:100", "21.45.9.1"},
		{"21.45.9.1, ", "101.1.0.4:100", "21.45.9.1"},
		{"192.168.5.45, 210.45.9.1, 89.5.6.1", "101.1.0.4:100", "210.45.9.1"},
		{"192.168.5.45, 172.24.9.1, 89.5.6.1", "101.1.0.4:100", "89.5.6.1"},
		{"192.168.5.45, 172.24.9.1", "101.1.0.4:100", "101.1.0.4"},
		{"192.168.5.45, 172.24.9.1", "101.1.0.4:5670", "101.1.0.4"},
	} {
		if actual := utils.ClientPublicIP(&http.Request{
			Header: http.Header{
				"X-Forwarded-For": []string{v.xForwardedFor},
			},
			RemoteAddr: v.remoteAddr,
		}); actual != v.expected {
			t.Errorf("IsxForwardedFor:%s, remoteAddr:%s, client ip Should Equal %s", v.xForwardedFor, v.remoteAddr, v.expected)
		}
	}

	r := &http.Request{Header: http.Header{}}
	r.Header.Set("X-Real-IP", " 10.10.10.10  ")
	r.Header.Set("X-Forwarded-For", " 172.17.40.152, 192.168.5.45")
	r.RemoteAddr = "40.40.40.40:42123 "

	if ip := utils.ClientPublicIP(r); ip != "40.40.40.40" {
		t.Errorf("actual:40.40.40.40, expected:%s", ip)
	}

	r.Header.Set("X-Real-IP", " 50.50.50.50  ")
	if ip := utils.ClientPublicIP(r); ip != "50.50.50.50" {
		t.Errorf("actual:50.50.50.50, expected:%s", ip)
	}

	r.Header.Del("X-Real-IP")
	r.Header.Del("X-Forwarded-For")
	r.RemoteAddr = "127.0.0.1:42123 "
	if ip := utils.ClientPublicIP(r); ip != "" {
		t.Errorf("ip: 127.0.0.1")
	}
}

func Test_Utils_IPString2Long(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "ip 字符串转换成数字: 127.0.0.1",
				Args: map[string]any{
					"ip": "127.0.0.1",
				},
				Want: map[string]any{
					"long":     uint(2130706433),
					"hasError": false,
				},
			},
			{
				Name: "ip 字符串转换成数字: 0.0.0.0",
				Args: map[string]any{
					"ip": "0.0.0.0",
				},
				Want: map[string]any{
					"long":     uint(0),
					"hasError": false,
				},
			},
			{
				Name: "ip 字符串转换成数字: 255.255.255.255",
				Args: map[string]any{
					"ip": "255.255.255.255",
				},
				Want: map[string]any{
					"long":     uint(4294967295),
					"hasError": false,
				},
			},
			{
				Name: "ip 字符串转换成数字: 192.168.1.1",
				Args: map[string]any{
					"ip": "192.168.1.1",
				},
				Want: map[string]any{
					"long":     uint(3232235777),
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			long, err := utils.IPString2Long(args["ip"].(string))
			if err != nil {
				res["long"] = nil
				res["hasError"] = true
			} else {
				res["long"] = long
				res["hasError"] = false
			}
			return res
		}).
		Done(t)
}

func Test_Utils_Long2IPString(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "数字转换成 ip 字符串: 127.0.0.1",
				Args: map[string]any{
					"long": uint(2130706433),
				},
				Want: map[string]any{
					"ip":       "127.0.0.1",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip 字符串: 0.0.0.0",
				Args: map[string]any{
					"long": uint(0),
				},
				Want: map[string]any{
					"ip":       "0.0.0.0",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip 字符串: 255.255.255.255",
				Args: map[string]any{
					"long": uint(4294967295),
				},
				Want: map[string]any{
					"ip":       "255.255.255.255",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip 字符串: 192.168.1.1",
				Args: map[string]any{
					"long": uint(3232235777),
				},
				Want: map[string]any{
					"ip":       "192.168.1.1",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			ip, err := utils.Long2IPString(args["long"].(uint))
			if err != nil {
				res["ip"] = nil
				res["hasError"] = true
			} else {
				res["ip"] = ip
				res["hasError"] = false
			}
			return res
		}).
		Done(t)
}

func Test_Utils_IP2Long(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "ip 转换成数字: 127.0.0.1",
				Args: map[string]any{
					"ip": "127.0.0.1",
				},
				Want: map[string]any{
					"long":     uint(2130706433),
					"hasError": false,
				},
			},
			{
				Name: "ip 转换成数字: 0.0.0.0",
				Args: map[string]any{
					"ip": "0.0.0.0",
				},
				Want: map[string]any{
					"long":     uint(0),
					"hasError": false,
				},
			},
			{
				Name: "ip 转换成数字: 255.255.255.255",
				Args: map[string]any{
					"ip": "255.255.255.255",
				},
				Want: map[string]any{
					"long":     uint(4294967295),
					"hasError": false,
				},
			},
			{
				Name: "ip 转换成数字: 192.168.1.1",
				Args: map[string]any{
					"ip": "192.168.1.1",
				},
				Want: map[string]any{
					"long":     uint(3232235777),
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			long, err := utils.IP2Long(net.ParseIP(args["ip"].(string)))
			if err != nil {
				res["long"] = nil
				res["hasError"] = true
			} else {
				res["long"] = long
				res["hasError"] = false
			}
			return res
		}).
		Done(t)
}

func Test_Utils_Long2IP(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "数字转换成 ip: 127.0.0.1",
				Args: map[string]any{
					"long": uint(2130706433),
				},
				Want: map[string]any{
					"ip":       "127.0.0.1",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip: 0.0.0.0",
				Args: map[string]any{
					"long": uint(0),
				},
				Want: map[string]any{
					"ip":       "0.0.0.0",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip: 255.255.255.255",
				Args: map[string]any{
					"long": uint(4294967295),
				},
				Want: map[string]any{
					"ip":       "255.255.255.255",
					"hasError": false,
				},
			},
			{
				Name: "数字转换成 ip: 192.168.1.1",
				Args: map[string]any{
					"long": uint(3232235777),
				},
				Want: map[string]any{
					"ip":       "192.168.1.1",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			ip, err := utils.Long2IP(args["long"].(uint))
			if err != nil {
				res["ip"] = nil
				res["hasError"] = true
			} else {
				res["ip"] = ip.String()
				res["hasError"] = false
			}
			return res
		}).
		Done(t)
}
