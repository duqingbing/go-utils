package test

import (
	"fmt"
	"reflect"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/utils"
)

func Test_Utils_IsZero(t *testing.T) {
	type x struct {
		Id int
	}
	var ptr *x
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串零值判断: 空字符",
				Args: map[string]any{
					"val": "",
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "数字零值判断: 0",
				Args: map[string]any{
					"val": 0,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "bool零值判断: false",
				Args: map[string]any{
					"val": false,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "结构体零值判断: false",
				Args: map[string]any{
					"val": x{},
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "指针零值判断: false",
				Args: map[string]any{
					"val": ptr,
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsZero(args["val"])
			return res
		}).
		Done(t)
}
func Test_Utils_IsNil(t *testing.T) {
	type x struct {
		Id int
	}
	var sPtr *x
	var dPtr *int

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通变量",
				Args: map[string]any{
					"val": "",
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "赋值的结构体",
				Args: map[string]any{
					"val": &x{},
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "nil 赋值",
				Args: map[string]any{
					"val": nil,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "未赋值普通变量",
				Args: map[string]any{
					"val": dPtr,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "未赋值结构体变量",
				Args: map[string]any{
					"val": sPtr,
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsNil(args["val"])
			return res
		}).
		Done(t)
}

func Test_Utils_IsStruct(t *testing.T) {
	type x struct {
	}

	valX := x{}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通变量",
				Args: map[string]any{
					"val": 11,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"val": &valX,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体",
				Args: map[string]any{
					"val": valX,
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsStruct(args["val"])
			return res
		}).
		Done(t)
}

func Test_Utils_IsPtr(t *testing.T) {
	type x struct {
	}

	valX := x{}
	valA := 11

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通变量",
				Args: map[string]any{
					"val": valA,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "普通变量指针",
				Args: map[string]any{
					"val": &valA,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "结构体",
				Args: map[string]any{
					"val": valX,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"val": &valX,
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsPtr(args["val"])
			return res
		}).
		Done(t)
}

func Test_Utils_IsReflectValue(t *testing.T) {
	type x struct {
	}

	valA := 11
	valX := x{}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通变量",
				Args: map[string]any{
					"val": valA,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "普通变量 -reflect",
				Args: map[string]any{
					"val": reflect.ValueOf(valA),
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "结构体",
				Args: map[string]any{
					"val": valX,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体 -reflect",
				Args: map[string]any{
					"val": reflect.ValueOf(valX),
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsReflectValue(args["val"])
			return res
		}).
		Done(t)
}

func Test_Utils_IsStructPtr(t *testing.T) {
	type x struct {
	}

	valX := x{}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通变量",
				Args: map[string]any{
					"val": 11,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体",
				Args: map[string]any{
					"val": valX,
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"val": &valX,
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IsStructPtr(args["val"])
			return res
		}).
		Done(t)
}

func Test_Utils_IF(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "真假判断",
				Args: map[string]any{
					"cond":     true,
					"trueVal":  "真",
					"falseVal": "假",
				},
				Want: map[string]any{
					"out": "真",
				},
			},
			{
				Name: "通用判断",
				Args: map[string]any{
					"cond":     false,
					"trueVal":  "通用",
					"falseVal": "不通用",
				},
				Want: map[string]any{
					"out": "不通用",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = utils.IF(args["cond"].(bool), args["trueVal"].(string), args["falseVal"].(string))
			return res
		}).
		Done(t)
}

func Test_Utils_Assert(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "bool-true 判断",
				Args: map[string]any{
					"express": true,
					"text":    "right value",
				},
				Want: map[string]any{
					"errorMsg": "",
				},
			},
			{
				Name: "bool-false 判断",
				Args: map[string]any{
					"express": false,
					"text":    "wrong value",
				},
				Want: map[string]any{
					"errorMsg": "wrong value",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["errorMsg"] = ""
			func() {
				defer func() {
					if err := recover(); err != nil {
						res["errorMsg"] = fmt.Sprintf("%+v", err)
					}
				}()
				utils.Assert(args["express"].(bool), args["text"].(string))
				fmt.Printf("\"%#v\" 没有发生 panic, 所以执行该条语句\n", args["express"])
			}()
			return res
		}).
		Done(t)
}
