package test

import (
	"fmt"
	"qingbing/xzutils/basic"
	"qingbing/xzutils/files"
	"testing"
)

func Test_Files_RmDir(t *testing.T) {
	file := "../temp_rm"
	err := files.RmDir(file, true)
	fmt.Println(err)
}

func Test_Files_FlushDir(t *testing.T) {
	file := "../temp_flush"
	err := files.FlushDir(file)
	fmt.Println(err)
}

func Test_Files_FileList(t *testing.T) {
	path := "."
	fileList, err := files.FileList(path, basic.KSlice{".git", ".gitignore"}, true)
	if err != nil {
		fmt.Println("获取文件列表失败")
	} else {
		fmt.Println(fileList)
	}
}

/*
func Test_Files_CopyDir(t *testing.T) {
	srcDir := "."
	distDir := "./test"
	err := files.CopyDir(srcDir, distDir, basic.KSlice{".git", "tmp"})
	if err != nil {
		fmt.Println("复制目录失败", err)
	} else {
		fmt.Printf("复制目录成功")
	}
}
*/
