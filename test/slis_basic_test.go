package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/slis"
)

func TestInSlice(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "slice contains true",
				Args: map[string]any{
					"obj":  []int{1, 2, 3},
					"item": 2,
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "slice contains false",
				Args: map[string]any{
					"obj":  []int{1, 2, 3},
					"item": 5,
				},
				Want: map[string]any{
					"out": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = slis.In(args["obj"].([]int), args["item"].(int))
			return res
		}).
		Done(t)
}
