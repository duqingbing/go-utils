package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/crypt"
)

func Test_Crypt_Md5(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串加密-111111",
				Args: basic.KMap{"str": "111111"},
				Want: basic.KMap{"res": "96e79218965eb72c92a549dd5a330112"},
			},
			{
				Name: "字符串加密-123456",
				Args: basic.KMap{"str": "123456"},
				Want: basic.KMap{"res": "e10adc3949ba59abbe56e057f20f883e"},
			},
			{
				Name: "字符串加密-666666",
				Args: basic.KMap{"str": "666666"},
				Want: basic.KMap{"res": "f379eaf3c831b04de153469d1bec345e"},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = crypt.Md5(args["str"].(string))
			return res
		}).
		Done(t)
}
