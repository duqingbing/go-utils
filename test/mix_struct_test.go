package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/mix"
)

func Test_Mix_StructToMap(t *testing.T) {
	type TName struct {
		Name string `json:"name"`
	}
	type TSex struct {
		Sex string `json:"sex"`
	}
	type TQq struct {
		Qq string `json:"qq"`
	}
	type TInfo struct {
		*TQq
		Id int `json:"id"`
	}

	type TPerson struct {
		Good  *string `json:"good"`
		Good1 string  `json:"good1"`
		*TName
		TSex
		TInfo
	}

	str := "ssss"
	obj := TPerson{
		Good:  &str,
		TName: &TName{Name: "qingbing"},
		TSex:  TSex{Sex: "male"},
		TInfo: TInfo{
			TQq: &TQq{
				Qq: "1111",
			},
			Id: 11111,
		},
	}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "结构体",
				Args: map[string]any{
					"obj": obj,
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"good":  "ssss",
						"good1": "",
						"name":  "qingbing",
						"sex":   "male",
						"qq":    "1111",
						"id":    11111,
					},
				},
			},
			{
				Name: "结构体指针",
				Args: map[string]any{
					"obj": obj,
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"good":  "ssss",
						"good1": "",
						"name":  "qingbing",
						"sex":   "male",
						"qq":    "1111",
						"id":    11111,
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = mix.StructToMap(args["obj"])
			return res
		}).
		Done(t)
}
