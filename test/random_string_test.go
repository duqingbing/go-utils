package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/random"
)

func Test_Random_String(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "随机字符串: 1-数字",
				Args: map[string]any{
					"len":  uint8(10),
					"mask": uint8(1),
				},
				Want: map[string]any{
					"isOk": true,
				},
			},
			{
				Name: "随机字符串: 2-小写字符",
				Args: map[string]any{
					"len":  uint8(10),
					"mask": uint8(2),
				},
				Want: map[string]any{
					"isOk": true,
				},
			},
			{
				Name: "随机字符串: 4-大写字符",
				Args: map[string]any{
					"len":  uint8(10),
					"mask": uint8(4),
				},
				Want: map[string]any{
					"isOk": true,
				},
			},
			{
				Name: "随机字符串: 7-数字,小写,大写字符",
				Args: map[string]any{
					"len":  uint8(10),
					"mask": uint8(7),
				},
				Want: map[string]any{
					"isOk": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			length := args["len"].(uint8)
			mask := args["mask"].(uint8)
			str := random.String(length, mask)
			fmt.Println(str)
			if len(str) == int(length) {
				res["isOk"] = true
			} else {
				res["isOk"] = false
			}
			return res
		}).
		Done(t)
}

func Test_Random_Uniqid(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "随机字符串 1",
				Args: map[string]any{},
				Want: map[string]any{
					"isOk": true,
				},
			},
			{
				Name: "随机字符串 2",
				Args: map[string]any{},
				Want: map[string]any{
					"isOk": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			str := random.Uniqid()
			fmt.Println(str)
			if len(str) == int(32) {
				res["isOk"] = true
			} else {
				res["isOk"] = false
			}
			return res
		}).
		Done(t)
}
