package test

import (
	"qingbing/xzutils/basic"
	"testing"
)

func sum(a, b int) int {
	return a + b
}

func Test_Basic_Testing(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "1+2",
				Args: basic.KMap{"a": 1, "b": 2},
				Want: basic.KMap{"res": 3},
			},
			{
				Name: "2+2=4",
				Args: basic.KMap{"a": 2, "b": 2},
				Want: basic.KMap{"res": 4},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = sum(args["a"].(int), args["b"].(int))
			return res
		}).
		Done(t)
}
