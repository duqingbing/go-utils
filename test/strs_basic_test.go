package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/strs"
)

func Test_Strs_IsFirstUpper(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "空字符",
				Args: map[string]any{
					"val": "",
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "非首字母大写",
				Args: map[string]any{
					"val": "qingbing",
				},
				Want: map[string]any{
					"out": false,
				},
			},
			{
				Name: "首字母大写",
				Args: map[string]any{
					"val": "Qing",
				},
				Want: map[string]any{
					"out": true,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = strs.IsFirstUpper(args["val"].(string))
			return res
		}).
		Done(t)
}

func Test_Strs_Hump(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "空字符",
				Args: map[string]any{
					"str":        " ",
					"firstUpper": true,
					"sep":        "_",
				},
				Want: map[string]any{
					"out": "",
				},
			},
			{
				Name: "分割符，头大写",
				Args: map[string]any{
					"str":        "___first__name__",
					"firstUpper": true,
					"sep":        "_",
				},
				Want: map[string]any{
					"out": "FirstName",
				},
			},
			{
				Name: "分割符, 小大写",
				Args: map[string]any{
					"str":        "___first__name__",
					"firstUpper": false,
					"sep":        "_",
				},
				Want: map[string]any{
					"out": "firstName",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = strs.Hump(args["str"].(string), args["firstUpper"].(bool), args["sep"].(string))
			return res
		}).
		Done(t)
}

func Test_Strs_Replace(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "不包裹字符串",
				Args: map[string]any{
					"str": "{{name}} is {{sex}}",
					"replaces": map[string]any{
						"name": "qingbing",
						"sex":  "male",
					},
					"wrap": []bool{false},
				},
				Want: map[string]any{
					"out": "{{qingbing}} is {{male}}",
				},
			},
			{
				Name: "包裹字符串",
				Args: map[string]any{
					"str": "{{name}} is {{sex}}",
					"replaces": map[string]any{
						"name": "qingbing",
						"sex":  "male",
					},
					"wrap": []bool{true},
				},
				Want: map[string]any{
					"out": "qingbing is male",
				},
			},
			{
				Name: "不设置包裹参数",
				Args: map[string]any{
					"str": "{{name}} is {{sex}}",
					"replaces": map[string]any{
						"name": "qingbing",
						"sex":  "male",
					},
					"wrap": []bool{},
				},
				Want: map[string]any{
					"out": "qingbing is male",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = strs.Replace(args["str"].(string), args["replaces"].(map[string]any), args["wrap"].([]bool)...)
			return res
		}).
		Done(t)
}

func Test_Strs_Join(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "普通连接",
				Args: map[string]any{
					"separator": "_",
					"strs":      []string{"hello", "world"},
				},
				Want: map[string]any{
					"out": "hello_world",
				},
			},
			{
				Name: "包含连接符的字符串连接",
				Args: map[string]any{
					"separator": "_",
					"strs":      []string{"__hello", "____world__"},
				},
				Want: map[string]any{
					"out": "hello_world",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = strs.Join(args["separator"].(string), args["strs"].([]string)...)
			return res
		}).
		Done(t)
}
