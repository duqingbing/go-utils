package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/maps"
)

type Addr struct {
	Address string
	ZipCode string
}

type person struct {
	Username string // key: Username
	RealName string `mapstructure:"real_name"` // real_name
	Age      uint8
	Addr     `mapstructure:",squash"` // squash 解析內嵌字段
}

var mStrong = map[string]interface{}{
	"Username":  "qing",
	"real_name": "bing",
	"Age":       uint8(11),
	"Address":   "chengdu",
	"ZipCode":   "61000",
}
var mWeak = map[string]interface{}{
	"Username":  "qing",
	"real_name": "bing",
	"Age":       "11",
	"Address":   "chengdu",
	"ZipCode":   "61000",
}
var sPerson = person{
	Username: "qing",
	RealName: "bing",
	Age:      11,
	Addr: Addr{
		Address: "chengdu",
		ZipCode: "61000",
	},
}

func Test_Maps_MapToStructure(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "Map字段弱对应-Weekly转换",
				Args: basic.KMap{"map": mWeak, "weakly": true},
				Want: basic.KMap{"person": sPerson, "hasErr": false},
			},
			{
				Name: "Map字段弱对应-Strong不转换",
				Args: basic.KMap{"map": mWeak},
				Want: basic.KMap{"person": nil, "hasErr": true},
			},
			{
				Name: "Map字段强对应",
				Args: basic.KMap{"map": mStrong},
				Want: basic.KMap{"person": sPerson, "hasErr": false},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			ms := maps.NewStructure()
			if args["weakly"] == true {
				ms.ConfWeak(true)
			}

			var p person
			err := ms.ToStruct(args["map"].(map[string]interface{}), &p)
			if err == nil {
				res["person"] = p
				res["hasErr"] = false
			} else {
				res["person"] = nil
				res["hasErr"] = true
			}
			return res
		}).
		Done(t)
}

func Test_Maps_ToMap(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "struct转换map",
				Args: basic.KMap{"person": sPerson},
				Want: basic.KMap{"map": mStrong, "hasErr": false},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			var m map[string]interface{}
			err := maps.NewStructure().ToMap(args["person"], &m)
			if err == nil {
				res["map"] = m
				res["hasErr"] = false
			} else {
				res["map"] = nil
				res["hasErr"] = true
			}
			return res
		}).
		Done(t)
}
