package test

import (
	"sort"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/maps"
)

func Test_Maps_In(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "map contains true",
				Args: map[string]any{
					"obj": map[string]interface{}{
						"a": "aa",
						"b": "bb",
					},
					"item": "aa",
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "map contains false",
				Args: map[string]any{
					"obj": map[string]interface{}{
						"a": "aa",
						"b": "bb",
					},
					"item": "cc",
				},
				Want: map[string]any{
					"out": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = maps.In(args["obj"].(map[string]interface{}), args["item"])
			return res
		}).
		Done(t)
}

func Test_Maps_Keys(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "数字map",
				Args: map[string]any{
					"obj": map[int]interface{}{
						101: "aa",
						102: "bb",
					},
				},
				Want: map[string]any{
					"out": []int{101, 102},
				},
			},
			{
				Name: "字符串map",
				Args: map[string]any{
					"obj": map[string]interface{}{
						"aaa": "aa",
						"bbb": "bb",
					},
				},
				Want: map[string]any{
					"out": []string{"aaa", "bbb"},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			switch args["obj"].(type) {
			case map[int]interface{}:
				keys := maps.Keys(args["obj"].(map[int]interface{}))
				sort.Ints(keys)
				res["out"] = keys
			case map[string]interface{}:
				keys := maps.Keys(args["obj"].(map[string]interface{}))
				sort.Strings(keys)
				res["out"] = keys
			}
			return res
		}).
		Done(t)
}

func Test_Maps_Exist(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "map exist true",
				Args: map[string]any{
					"obj": map[string]interface{}{
						"a": "aa",
						"b": "bb",
					},
					"key": "a",
				},
				Want: map[string]any{
					"out": true,
				},
			},
			{
				Name: "map exist false",
				Args: map[string]any{
					"obj": map[string]interface{}{
						"a": "aa",
						"b": "bb",
					},
					"key": "c",
				},
				Want: map[string]any{
					"out": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["out"] = maps.Exist(args["obj"].(map[string]interface{}), args["key"].(string))
			return res
		}).
		Done(t)
}
