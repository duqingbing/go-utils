package test

import (
	"qingbing/xzutils/basic"
	"testing"
)

func Test_Types_StructMapToSlice(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "结构map转换成字符串切片",
				Args: basic.KMap{"map": basic.KStructMap{"qing": basic.EStruct{}, "bing": struct{}{}}},
				Want: basic.KMap{"res": basic.KSlice{"qing", "bing"}},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = args["map"].(basic.KStructMap).ToSlice()
			return res
		}).
		Done(t)
}

func Test_Types_SliceToStructMap(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串切片转换成结构map",
				Args: basic.KMap{"slice": basic.KSlice{"qing", "bing"}},
				Want: basic.KMap{"res": basic.KStructMap{"qing": basic.EStruct{}, "bing": basic.EStruct{}}},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = args["slice"].(basic.KSlice).ToMap()
			return res
		}).
		Done(t)
}
