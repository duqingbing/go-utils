package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/crypt"
	"qingbing/xzutils/slis"
	"qingbing/xzutils/tools"
)

func TestTreeMap(t *testing.T) {
	tree := tools.NewParentTree()
	data := tree.
		SetIdFlag("id").
		SetPidFlag("pid").
		SetRootVal(0).
		SetLabelCall(func(data map[string]any) string {
			return fmt.Sprintf("%v - %v", data["id"], data["name"])
		}).
		SetLeafShowCall(func(data map[string]any) bool {
			return slis.In([]int{1, 7, 5, 6}, data["id"].(int))
		}).
		SetData([]map[string]any{
			{"id": 1, "pid": 0, "name": "aa"},
			{"id": 2, "pid": 0, "name": "bb"},
			{"id": 3, "pid": 1, "name": "cc"},
			{"id": 4, "pid": 1, "name": "dd"},
			{"id": 5, "pid": 2, "name": "ee"},
			{"id": 6, "pid": 2, "name": "ff"},
			{"id": 7, "pid": 4, "name": "gg"},
			{"id": 8, "pid": 5, "name": "hh"},
		}).GetTreeData()
	str, _ := crypt.JsonEncode(data)
	fmt.Printf("%s\n", str)
}
