package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/random"
)

func Test_Random_Int(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "随机10以内",
				Args: map[string]any{
					"max": 10,
				},
				Want: map[string]any{
					"err": nil,
				},
			},
			{
				Name: "随机100以内",
				Args: map[string]any{
					"max": 100,
				},
				Want: map[string]any{
					"err": nil,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			max := args["max"].(int)
			out := random.Int(max)
			// fmt.Println(out)
			if out < 0 || out >= max {
				res["err"] = fmt.Errorf("结果\"%d\"超出范围: [%d, %d)", out, 0, max)
			} else {
				res["err"] = nil
			}
			return res
		}).
		Done(t)
}

func Test_Random_Range(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "随机[2, 5)之间的整数",
				Args: map[string]any{
					"max": 5,
					"min": 2,
				},
				Want: map[string]any{
					"err": nil,
				},
			},
			{
				Name: "随机[80, 100)之间的整数",
				Args: map[string]any{
					"max": 100,
					"min": 80,
				},
				Want: map[string]any{
					"err": nil,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			max := args["max"].(int)
			min := args["min"].(int)
			out := random.Range(min, max)
			// fmt.Println(out)
			if out < min || out >= max {
				res["err"] = fmt.Errorf("结果\"%d\"超出范围: [%d, %d)", out, min, max)
			} else {
				res["err"] = nil
			}
			return res
		}).
		Done(t)
}
