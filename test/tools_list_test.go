package test

import (
	"fmt"
	"qingbing/xzutils/tools"
	"testing"
)

func TestXzList(t *testing.T) {
	list := tools.NewList[string]()
	// Push
	list.Push("test", "bing").
		Push("qing")
	list.Print()

	// Pop
	el := list.Pop()
	fmt.Println(el)
	list.Print()

	// Shift
	el = list.Shift()
	fmt.Println(el)
	el = list.Shift()
	fmt.Println(el)
	el = list.Pop()
	fmt.Println(el)
	list.Print()

	// Unshift
	list.UnShift("test").
		UnShift("bing", "qing")
	list.Print()
}
