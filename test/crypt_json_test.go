package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/crypt"
)

func Test_Crypt_JsonEncode(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "Json-bool",
				Args: basic.KMap{"data": true},
				Want: basic.KMap{"res": "true", "err": nil},
			},
			{
				Name: "Json-number",
				Args: basic.KMap{"data": 11},
				Want: basic.KMap{"res": "11", "err": nil},
			},
			{
				Name: "Json-string",
				Args: basic.KMap{"data": "hello"},
				Want: basic.KMap{"res": "\"hello\"", "err": nil},
			},
			{
				Name: "Json-map",
				Args: basic.KMap{"data": map[string]interface{}{"name": "qingbing"}},
				Want: basic.KMap{"res": "{\"name\":\"qingbing\"}", "err": nil},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			str, err := crypt.JsonEncode(args["data"])
			if err == nil {
				res["res"] = str
				res["err"] = nil
			} else {
				res["res"] = nil
				res["err"] = err
			}
			return res
		}).
		Done(t)
}

func Test_Crypt_JsonDecode(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "Json-bool",
				Args: basic.KMap{"data": "true"},
				Want: basic.KMap{"res": true, "err": nil},
			},
			{
				Name: "Json-number",
				Args: basic.KMap{"data": "11"},
				Want: basic.KMap{"res": float64(11), "err": nil}, // 默认解析 出来时 float64 类型
			},
			{
				Name: "Json-string",
				Args: basic.KMap{"data": "\"hello\""},
				Want: basic.KMap{"res": "hello", "err": nil},
			},
			{
				Name: "Json-map",
				Args: basic.KMap{"data": "{\"name\":\"qingbing\"}"},
				Want: basic.KMap{"res": map[string]interface{}{"name": "qingbing"}, "err": nil},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			var obj any
			err := crypt.JsonDecode(args["data"].(string), &obj)
			if err == nil {
				res["res"] = obj
				res["err"] = nil
			} else {
				res["res"] = nil
				res["err"] = err
			}
			return res
		}).
		Done(t)
}
