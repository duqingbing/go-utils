package test

import (
	"fmt"
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/utils"
)

func Test_Utilsss_FillWidth(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "前位0补足: 4 位",
				Args: map[string]any{
					"str":   "ff0",
					"width": 4,
				},
				Want: map[string]any{
					"str": "0ff0",
				},
			},
			{
				Name: "前位0补足: 8 位",
				Args: map[string]any{
					"str":   "ff0",
					"width": 8,
				},
				Want: map[string]any{
					"str": "00000ff0",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["str"] = utils.FillWidth(args["str"].(string), args["width"].(int))
			return res
		}).
		Done(t)
}

func Test_Utilsss_HexToBinary(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "十六进制转二进制: 不合法",
				Args: map[string]any{
					"hex": "qing",
				},
				Want: map[string]any{
					"bin":      nil,
					"hasError": true,
				},
			},
			{
				Name: "十六进制转二进制: 71696E67",
				Args: map[string]any{
					"hex": "71696E6771696E6771696E67",
				},
				Want: map[string]any{
					"bin":      "011100010110100101101110011001110111000101101001011011100110011101110001011010010110111001100111",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			bin, err := utils.HexToBinary(args["hex"].(string))
			// fmt.Println(bin, err)
			if err == nil {
				res["bin"] = bin
				res["hasError"] = false
			} else {
				res["bin"] = nil
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}
func Test_Utilsss_BinaryToHex(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "二进制转十进制: qing",
				Args: map[string]any{
					"bin": "011100010110100101101110011001110111000101101001011011100110011101110001011010010110111001100111",
				},
				Want: map[string]any{
					"hex":      "71696E6771696E6771696E67",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			hex, err := utils.BinaryToHex(args["bin"].(string))
			fmt.Println(len(args["bin"].(string)), hex, err)
			if err == nil {
				res["hex"] = hex
				res["hasError"] = false
			} else {
				res["hex"] = nil
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}

func Test_Utilsss_StringToHex(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串 转换成 十六进制: qing",
				Args: map[string]any{
					"str": "qing",
				},
				Want: map[string]any{
					"hex": "71696e67",
				},
			},
			{
				Name: "字符串 转换成 十六进制: 小Z管理系统来袭",
				Args: map[string]any{
					"str": "小Z管理系统来袭",
				},
				Want: map[string]any{
					"hex": "e5b08f5ae7aea1e79086e7b3bbe7bb9fe69da5e8a2ad",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["hex"] = utils.StringToHex(args["str"].(string))
			return res
		}).
		Done(t)
}

func Test_Utilsss_HexToString(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "十六进制 转换成 字符串: qing",
				Args: map[string]any{
					"hex": "71696e67",
				},
				Want: map[string]any{
					"str":      "qing",
					"hasError": false,
				},
			},
			{
				Name: "十六进制 转换成 字符串: 小Z管理系统来袭",
				Args: map[string]any{
					"hex": "e5b08f5ae7aea1e79086e7b3bbe7bb9fe69da5e8a2ad",
				},
				Want: map[string]any{
					"str":      "小Z管理系统来袭",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			str, err := utils.HexToString(args["hex"].(string))
			// fmt.Println(str, err)
			if err == nil {
				res["str"] = str
				res["hasError"] = false
			} else {
				res["str"] = nil
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}

func Test_Utilsss_StringToBinary(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串 转换成 二进制: qing",
				Args: map[string]any{
					"str": "qing",
				},
				Want: map[string]any{
					"bin":      "01110001011010010110111001100111",
					"hasError": false,
				},
			},
			{
				Name: "字符串 转换成 二进制: 小Z管理系统来袭",
				Args: map[string]any{
					"str": "小Z管理系统来袭",
				},
				Want: map[string]any{
					"bin":      "11100101101100001000111101011010111001111010111010100001111001111001000010000110111001111011001110111011111001111011101110011111111001101001110110100101111010001010001010101101",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			bin, err := utils.StringToBinary(args["str"].(string))
			// fmt.Println(bin, err)
			if err == nil {
				res["bin"] = bin
				res["hasError"] = false
			} else {
				res["bin"] = nil
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}

func Test_Utilsss_BinaryToString(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "字符串 转换成 二进制: qing",
				Args: map[string]any{
					"bin": "01110001011010010110111001100111",
				},
				Want: map[string]any{
					"str":      "qing",
					"hasError": false,
				},
			},
			{
				Name: "字符串 转换成 二进制: 小Z管理系统来袭",
				Args: map[string]any{
					"bin": "11100101101100001000111101011010111001111010111010100001111001111001000010000110111001111011001110111011111001111011101110011111111001101001110110100101111010001010001010101101",
				},
				Want: map[string]any{
					"str":      "小Z管理系统来袭",
					"hasError": false,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			str, err := utils.BinaryToString(args["bin"].(string))
			// fmt.Println(str, err)
			if err == nil {
				res["str"] = str
				res["hasError"] = false
			} else {
				res["str"] = nil
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}
