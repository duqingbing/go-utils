package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/mix"
)

type map_a struct {
	Name string `json:"name"`
}

func Test_Mix_Mixins(t *testing.T) {
	type MapB struct {
		B string `json:"b"`
	}
	type MapC struct {
		MapB
		C string `json:"c"`
	}

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "map + map 合并",
				Args: map[string]any{
					"mapA": map[string]interface{}{
						"a": "a",
					},
					"MapB": map[string]interface{}{
						"b": "b",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"a": "a",
						"b": "b",
					},
				},
			},
			{
				Name: "map + struct 合并",
				Args: map[string]any{
					"mapA": map[string]interface{}{
						"a": "a",
					},
					"MapB": MapB{
						B: "b",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"a": "a",
						"b": "b",
					},
				},
			},
			{
				Name: "map + structPtr 合并",
				Args: map[string]any{
					"mapA": map[string]interface{}{
						"a": "a",
					},
					"MapB": &MapB{
						B: "b",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"a": "a",
						"b": "b",
					},
				},
			},
			{
				Name: "map + structCom 合并",
				Args: map[string]any{
					"mapA": map[string]interface{}{
						"a": "a",
					},
					"MapB": &MapC{
						MapB: MapB{
							B: "b",
						},
						C: "c",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"a": "a",
						"b": "b",
						"c": "c",
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			mix.Mixins(args["mapA"].(map[string]interface{}), args["MapB"])
			res["out"] = args["mapA"]
			return res
		}).
		Done(t)
}
