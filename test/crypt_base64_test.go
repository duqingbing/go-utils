package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/crypt"
)

func Test_Crypt_Base64Encode(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "Base64-111111",
				Args: basic.KMap{"data": "111111"},
				Want: basic.KMap{"res": "MTExMTEx"},
			},
			{
				Name: "Base64-123456",
				Args: basic.KMap{"data": "123456"},
				Want: basic.KMap{"res": "MTIzNDU2"},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = crypt.Base64Encode(args["data"].(string))
			return res
		}).
		Done(t)
}

func Test_Crypt_Base64Decode(t *testing.T) {
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "Base64-111111",
				Args: basic.KMap{"data": "MTExMTEx"},
				Want: basic.KMap{"res": "111111", "hasError": false},
			},
			{
				Name: "Base64-123456",
				Args: basic.KMap{"data": "MTIzNDU2"},
				Want: basic.KMap{"res": "123456", "hasError": false},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			str, err := crypt.Base64Decode(args["data"].(string))
			if err == nil {
				res["res"] = str
				res["hasError"] = false
			} else {
				res["res"] = ""
				res["hasError"] = true
			}
			return res
		}).
		Done(t)
}
