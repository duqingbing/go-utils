package test

import (
	"testing"

	"qingbing/xzutils/basic"
	"qingbing/xzutils/mix"
)

func TestGetKey(t *testing.T) {
	ms := mix.NewStore()
	ms.SetKey("name", "qingbing")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "获取不存在, 返回默认",
				Args: map[string]any{
					"key":        "noExistKey",
					"defaultVal": "defaultValue",
				},
				Want: map[string]any{
					"out": "defaultValue",
				},
			},
			{
				Name: "获取返回设置值",
				Args: map[string]any{
					"key":        "name",
					"defaultVal": "defaultValue",
				},
				Want: map[string]any{
					"out": "qingbing",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetKey(key, args["defaultVal"])
			return res
		}).
		Done(t)
}

func TestSetKey(t *testing.T) {
	ms := mix.NewStore()

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置字符串",
				Args: map[string]any{
					"key": "name",
					"val": "qing",
				},
				Want: map[string]any{
					"out": "qing",
				},
			},
			{
				Name: "设置map",
				Args: map[string]any{
					"key": "name",
					"val": map[string]interface{}{
						"name": "qing",
						"sex":  "male",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qing",
						"sex":  "male",
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.SetKey(key, args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetKey(key, nil)
			return res
		}).
		Done(t)
}

func TestGetAll(t *testing.T) {
	ms := mix.NewStore()

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置字符串",
				Args: map[string]any{
					"key": "name",
					"val": "qingbing",
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qingbing",
					},
				},
			},
			{
				Name: "设置map",
				Args: map[string]any{
					"key": "info",
					"val": map[string]interface{}{
						"name": "qingbing",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qingbing",
						"info": map[string]interface{}{
							"name": "qingbing",
						},
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			//  逻辑设置
			ms.SetKey(args["key"].(string), args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = *(ms.GetAll())
			return res
		}).
		Done(t)
}

func TestSetKeyIfNotExist(t *testing.T) {
	ms := mix.NewStore()
	ms.SetKey("name", "qingbing")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置不存在字符串",
				Args: map[string]any{
					"key": "sex",
					"val": "female",
				},
				Want: map[string]any{
					"out": "female",
				},
			},
			{
				Name: "设置存在字符串",
				Args: map[string]any{
					"key": "name",
					"val": "female",
				},
				Want: map[string]any{
					"out": "qingbing",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.SetKeyIfNotExist(key, args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetKey(key, nil)
			return res
		}).
		Done(t)
}

func TestDelKey(t *testing.T) {
	ms := mix.NewStore()
	ms.SetKey("name", "qingbing")
	ms.SetKey("sex", "male")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "删除不存在字符串",
				Args: map[string]any{
					"key": "info",
				},
				Want: map[string]any{
					"out": nil,
				},
			},
			{
				Name: "删除存在字符串",
				Args: map[string]any{
					"key": "name",
				},
				Want: map[string]any{
					"out": nil,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.DelKey(key)
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetKey(key, nil)
			return res
		}).
		Done(t)

	// fmt.Printf("== data: %+v", *(ms.GetAll()))
}

func TestClear(t *testing.T) {
	ms := mix.NewStore()
	ms.SetKey("name", "qingbing")
	ms.SetKey("sex", "male")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "清空所有普通数据",
				Want: map[string]any{
					"out": map[string]interface{}{},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			//  逻辑设置
			ms.Clear()
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = *(ms.GetAll())
			return res
		}).
		Done(t)

	// fmt.Printf("== data: %+v", *(ms.GetAll()))
}

func TestGetMapKey(t *testing.T) {
	mKey := "info"

	ms := mix.NewStore()
	ms.SetMapKey(mKey, "name", "qingbing")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "获取不存在, 返回默认",
				Args: map[string]any{
					"key":        "noExistKey",
					"defaultVal": "defaultValue",
				},
				Want: map[string]any{
					"out": "defaultValue",
				},
			},
			{
				Name: "获取返回设置值",
				Args: map[string]any{
					"key":        "name",
					"defaultVal": "defaultValue",
				},
				Want: map[string]any{
					"out": "qingbing",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetMapKey(mKey, key, args["defaultVal"])
			return res
		}).
		Done(t)

	// fmt.Printf("== mData: %#v", *(ms.GetMapAll(mKey)))
}

func TestSetMapKey(t *testing.T) {
	mKey := "info"
	ms := mix.NewStore()

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置字符串",
				Args: map[string]any{
					"key": "name",
					"val": "qing",
				},
				Want: map[string]any{
					"out": "qing",
				},
			},
			{
				Name: "设置map",
				Args: map[string]any{
					"key": "info",
					"val": map[string]interface{}{
						"name": "qing",
						"sex":  "male",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qing",
						"sex":  "male",
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.SetMapKey(mKey, key, args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetMapKey(mKey, key, nil)
			return res
		}).
		Done(t)

	// fmt.Printf("== mData: %#v", *(ms.GetMapAll(mKey)))
}

func TestGetMapAll(t *testing.T) {
	mKey := "info"
	ms := mix.NewStore()

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置字符串",
				Args: map[string]any{
					"key": "name",
					"val": "qingbing",
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qingbing",
					},
				},
			},
			{
				Name: "设置map",
				Args: map[string]any{
					"key": "info",
					"val": map[string]interface{}{
						"name": "qingbing",
					},
				},
				Want: map[string]any{
					"out": map[string]interface{}{
						"name": "qingbing",
						"info": map[string]interface{}{
							"name": "qingbing",
						},
					},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			//  逻辑设置
			ms.SetMapKey(mKey, args["key"].(string), args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = *(ms.GetMapAll(mKey))
			return res
		}).
		Done(t)
	// fmt.Printf("== mData: %#v", *(ms.GetMapAll(mKey)))
}

func TestSetMapKeyIfNotExist(t *testing.T) {
	mKey := "info"

	ms := mix.NewStore()
	ms.SetMapKey(mKey, "name", "qingbing")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "设置不存在字符串",
				Args: map[string]any{
					"key": "sex",
					"val": "female",
				},
				Want: map[string]any{
					"out": "female",
				},
			},
			{
				Name: "设置存在字符串",
				Args: map[string]any{
					"key": "name",
					"val": "female",
				},
				Want: map[string]any{
					"out": "qingbing",
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.SetMapKeyIfNotExist(mKey, key, args["val"])
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetMapKey(mKey, key, nil)
			return res
		}).
		Done(t)
	// fmt.Printf("== mData: %#v", *(ms.GetMapAll(mKey)))
}

func TestDelMapKey(t *testing.T) {
	mKey := "info"

	ms := mix.NewStore()
	ms.SetMapKey(mKey, "name", "qingbing")
	ms.SetMapKey(mKey, "sex", "male")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "删除不存在字符串",
				Args: map[string]any{
					"key": "info",
				},
				Want: map[string]any{
					"out": nil,
				},
			},
			{
				Name: "删除存在字符串",
				Args: map[string]any{
					"key": "name",
				},
				Want: map[string]any{
					"out": nil,
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			// 参数获取
			key := args["key"].(string)
			//  逻辑设置
			ms.DelMapKey(mKey, key)
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = ms.GetMapKey(mKey, key, nil)
			return res
		}).
		Done(t)

	// fmt.Printf("== mData: %#v", *(ms.GetMapAll(mKey)))
}

func TestMapClear(t *testing.T) {
	mKey := "info"

	ms := mix.NewStore()
	ms.SetMapKey(mKey, "name", "qingbing")
	ms.SetMapKey(mKey, "sex", "male")

	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "清空存在的 map",
				Args: map[string]interface{}{
					"key": "info",
				},
				Want: map[string]any{
					"out": map[string]interface{}{},
				},
			},
			{
				Name: "清空不存在的 map",
				Args: map[string]interface{}{
					"key": "noExist",
				},
				Want: map[string]any{
					"out": map[string]interface{}{},
				},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			//  逻辑设置
			ms.MapClear(args["key"].(string))
			// 结果对比设置
			res := make(basic.KMap, 0)
			res["out"] = *(ms.GetMapAll(args["key"].(string)))
			return res
		}).
		Done(t)

	// fmt.Printf("== mData: %#v", *(ms))
}
