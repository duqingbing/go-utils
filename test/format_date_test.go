package test

import (
	"qingbing/xzutils/basic"
	"qingbing/xzutils/formats"
	"testing"
	"time"
)

func Test_Format_FormatDate(t *testing.T) {
	testTimeStr, _ := time.Parse("2006-01-02 15:04:05", "2024-11-11 13:47:07")
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "时间到秒",
				Args: basic.KMap{"date": testTimeStr, "format": "2006-01-02 15:04:05"},
				Want: basic.KMap{"res": "2024-11-11 13:47:07"},
			},
			{
				Name: "时间到分",
				Args: basic.KMap{"date": testTimeStr, "format": "2006-01-02 15:04"},
				Want: basic.KMap{"res": "2024-11-11 13:47"},
			},
			{
				Name: "时间到小时",
				Args: basic.KMap{"date": testTimeStr, "format": "2006-01-02 15"},
				Want: basic.KMap{"res": "2024-11-11 13"},
			},
			{
				Name: "时间到日期",
				Args: basic.KMap{"date": testTimeStr, "format": "2006-01-02"},
				Want: basic.KMap{"res": "2024-11-11"},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = formats.FormatDate(args["date"].(time.Time), args["format"].(string))
			return res
		}).
		Done(t)
}

func Test_Format_Date(t *testing.T) {
	testTimeStr, _ := time.Parse("2006-01-02 15:04:05", "2024-11-11 13:47:07")
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "指定时间",
				Args: basic.KMap{"arg": testTimeStr},
				Want: basic.KMap{"res": "2024-11-11"},
			},
			{
				Name: "指定格式化",
				Args: basic.KMap{"arg": "2024-11-11 13"},
				Want: basic.KMap{"res": time.Now().Format("2024-11-11 13")},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = formats.Date(args["arg"])
			return res
		}).
		Done(t)
}

func Test_Format_Datetime(t *testing.T) {
	testTimeStr, _ := time.Parse("2006-01-02 15:04:05", "2024-11-11 13:47:07")
	basic.NewTesting().
		SetCases([]basic.XzTestingCase{
			{
				Name: "指定时间",
				Args: basic.KMap{"arg": testTimeStr},
				Want: basic.KMap{"res": "2024-11-11 13:47:07"},
			},
			{
				Name: "指定格式化",
				Args: basic.KMap{"arg": "2024-11-11 13"},
				Want: basic.KMap{"res": time.Now().Format("2024-11-11 13")},
			},
		}).
		SetLogic(func(args basic.KMap) basic.KMap {
			res := make(basic.KMap, 0)
			res["res"] = formats.Datetime(args["arg"])
			return res
		}).
		Done(t)
}
