package test

import (
	"fmt"
	"io"
	"qingbing/xzutils/types"
	"testing"
)

func Test_types_discardWriter(t *testing.T) {
	var w io.Writer = &types.DiscardWriter{}
	num, err := w.Write([]byte("hello, world!"))
	fmt.Println(num, err)
}
